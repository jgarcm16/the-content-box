-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: TheContentBox
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cat_noticias`
--

DROP TABLE IF EXISTS `cat_noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cat_noticias` (
  `categoria_id` int NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`categoria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_noticias`
--

LOCK TABLES `cat_noticias` WRITE;
/*!40000 ALTER TABLE `cat_noticias` DISABLE KEYS */;
INSERT INTO `cat_noticias` VALUES (1,'Exitos'),(2,'Novedades'),(3,'Tops'),(4,'Nunca mueren'),(5,'Fracasos');
/*!40000 ALTER TABLE `cat_noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favoritos`
--

DROP TABLE IF EXISTS `favoritos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favoritos` (
  `IdFavorito` int NOT NULL AUTO_INCREMENT,
  `Comentario` text,
  `Valoracion` int DEFAULT NULL,
  `IdProducto` int DEFAULT NULL,
  `IdUsuario` int DEFAULT NULL,
  PRIMARY KEY (`IdFavorito`),
  KEY `FK_favoritos_IdProducto` (`IdProducto`),
  KEY `FK_favoritos_IdUsuario` (`IdUsuario`),
  CONSTRAINT `FK_favoritos_IdProducto` FOREIGN KEY (`IdProducto`) REFERENCES `productos` (`IdProducto`),
  CONSTRAINT `FK_favoritos_IdUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favoritos`
--

LOCK TABLES `favoritos` WRITE;
/*!40000 ALTER TABLE `favoritos` DISABLE KEYS */;
INSERT INTO `favoritos` VALUES (1,'Mi canción favorita de C.Tangana',10,1510554804,10),(2,'Me encanta Quentin Tarantino',7,597132157,10),(11,'Me gusta mucho ',7,665143133,10),(12,'Me cae un poco mal',1,128497079,10);
/*!40000 ALTER TABLE `favoritos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensajes`
--

DROP TABLE IF EXISTS `mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mensajes` (
  `IdMensaje` int NOT NULL AUTO_INCREMENT,
  `ContMensaje` text NOT NULL,
  `IdUsuaOrigen` int NOT NULL,
  `IdUsuaDestino` int NOT NULL,
  `Estado` tinyint NOT NULL,
  `Asunto` text NOT NULL,
  `Fecha` date DEFAULT NULL,
  PRIMARY KEY (`IdMensaje`),
  UNIQUE KEY `IdMensajes_UNIQUE` (`IdMensaje`),
  KEY `FK_Mensaje_UsuarioOri` (`IdUsuaOrigen`),
  KEY `FK_Mensaje_UsuarioDes` (`IdUsuaDestino`),
  CONSTRAINT `FK_Mensaje_UsuarioDes` FOREIGN KEY (`IdUsuaDestino`) REFERENCES `usuarios` (`IdUsuario`),
  CONSTRAINT `FK_Mensaje_UsuarioOri` FOREIGN KEY (`IdUsuaOrigen`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensajes`
--

LOCK TABLES `mensajes` WRITE;
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
INSERT INTO `mensajes` VALUES (1,'prueba1',1,10,1,'prueba','2020-05-29'),(2,'prueba2',1,10,1,'prueba','2020-05-29'),(3,'Hola Marco, este es un mensaje de prueba. Espero que te llegue...\r\n\r\nUn saludo!',1,10,1,'Mensaje de prueba','2020-05-29'),(4,'Hola Marco, este es un mensaje de prueba. Espero que te llegue!\r\n\r\nUn saludo.',1,10,1,'Segundo mensaje de prueba','2020-05-29'),(5,'Este mensaje lleva muchos caracteres y deberá estar acordado en la página principal del menú de entrada. Son muchos caracteres así que no entrará entero.',10,10,1,'Mensaje de prueba de fecha con muchos caracteres','2020-05-29'),(6,'esto es el mensaje de prueba edu 1',1,10,1,'prueba edu1','2020-05-29'),(7,'mensaje prueba2 edu',10,1,1,'prueba2 edu','2020-05-29'),(8,'prueba con mensaje enviado correctamente al enviar',1,10,0,'prueba con mensaje enviado','2020-05-29'),(9,'hola espero que te llegue el mensaje por que  existes',10,3,0,'prueba3 edu','2020-05-29');
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `IdMenu` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Tipo` enum('S','I') NOT NULL,
  `Estado` bit(1) NOT NULL DEFAULT b'1',
  `URL` varchar(200) DEFAULT NULL,
  `IdPadre` int DEFAULT NULL,
  `IdRol` int DEFAULT NULL,
  PRIMARY KEY (`IdMenu`),
  UNIQUE KEY `IdMenu_UNIQUE` (`IdMenu`),
  KEY `FK_IdPadre` (`IdPadre`),
  KEY `FK_IdRol` (`IdRol`),
  CONSTRAINT `FK_IdPadre` FOREIGN KEY (`IdPadre`) REFERENCES `menus` (`IdMenu`),
  CONSTRAINT `FK_IdRol` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`IdRol`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Usuario','S',_binary '',NULL,NULL,1),(2,'Modificar datos','I',_binary '','/private/usuario/modificarDatosUsuario',1,1),(3,'Gestión de usuarios','S',_binary '',NULL,NULL,3),(4,'Crear usuario','I',_binary '','/private/administrador/crearUsuario',3,3),(5,'Bloquear/desbloquear usuarios','I',_binary '','/private/administrador/bloquearUsuario',3,3),(6,'Ingresar/retirar boxes','I',_binary '','/private/administrador/ingresarBoxes',3,3),(7,'Productos','S',_binary '',NULL,NULL,1),(8,'Lista favoritos','I',_binary '','/private/usuario/listaFavoritos',7,1),(9,'Mis productos','I',_binary '','/private/usuario/misProductos',7,1),(10,'Todos los productos','I',_binary '','/private/usuario/todosProductos',7,1),(12,'Modificar usuarios','I',_binary '','/private/administrador/modificarDatosUsuario',3,3),(13,'Modificar Productos','I',_binary '','/private/administrador/modificarProductos',14,3),(14,'Gestión de Productos','S',_binary '',NULL,NULL,3),(15,'Mensajes','S',_binary '',NULL,NULL,1),(16,'Buzón de entrada','I',_binary '','/private/usuario/buzonDeEntrada',15,1),(17,'Escribir mensaje','I',_binary '','/private/usuario/escribirMensaje',15,1),(19,'Mensajes','S',_binary '',NULL,NULL,3),(20,'Centro de mensajes','I',_binary '','/private/administrador/centroDeMensajes',19,3),(21,'Buzon de entrada ','I',_binary '','/private/usuario/buzonDeEntrada',19,3),(22,'Escribir mensaje','I',_binary '','/private/usuario/escribirMensaje',19,3),(23,'Noticias','S',_binary '',NULL,NULL,1),(24,'Ver noticias','I',_binary '','/private/usuario/noticias',23,1),(25,'Gestión de noticias','S',_binary '',NULL,NULL,3),(26,'Crear noticias','I',_binary '','/private/administrador/crearNoticias',25,3),(27,'Gestión de productos','S',_binary '',NULL,NULL,3),(28,'Crear productos','I',_binary '','/private/administrador/crearProductos',27,3);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misproductos`
--

DROP TABLE IF EXISTS `misproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `misproductos` (
  `IdMisProductos` int NOT NULL AUTO_INCREMENT,
  `IdUsuario` int NOT NULL,
  `IdProducto` int NOT NULL,
  `FechaAdquisicion` date NOT NULL,
  `FechaExpiracion` date DEFAULT NULL,
  `Estado` tinyint NOT NULL,
  `Alquilado` tinyint NOT NULL,
  `DiasAlquiler` int DEFAULT NULL,
  PRIMARY KEY (`IdMisProductos`),
  UNIQUE KEY `miProducto` (`IdUsuario`,`IdProducto`),
  KEY `IdUsuario` (`IdUsuario`),
  KEY `IdProducto` (`IdProducto`),
  CONSTRAINT `IdProducto` FOREIGN KEY (`IdProducto`) REFERENCES `productos` (`IdProducto`),
  CONSTRAINT `IdUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misproductos`
--

LOCK TABLES `misproductos` WRITE;
/*!40000 ALTER TABLE `misproductos` DISABLE KEYS */;
INSERT INTO `misproductos` VALUES (1,10,1510066669,'2020-05-24',NULL,1,0,0),(2,10,1510885042,'2020-05-24','2020-05-08',0,1,15),(3,10,1511650858,'2020-05-24',NULL,1,0,0),(4,10,1496062625,'2020-05-24','2020-05-31',0,1,7),(5,10,665143133,'2020-05-24',NULL,1,0,0),(6,10,1510554804,'2020-05-24',NULL,1,0,0),(7,10,1511586138,'2020-05-25',NULL,1,0,0),(8,10,391465654,'2020-05-25',NULL,1,0,0),(9,10,1489425621,'2020-05-31',NULL,1,0,0),(10,10,1510840040,'2020-05-31',NULL,1,0,0);
/*!40000 ALTER TABLE `misproductos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `noticias` (
  `noticias_id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(2000) DEFAULT NULL,
  `estado` bit(1) NOT NULL,
  `fecha_publicacion` date DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  `categoria_id` int NOT NULL,
  `usuarios_id` int NOT NULL,
  PRIMARY KEY (`noticias_id`),
  KEY `usuarios_id` (`usuarios_id`),
  KEY `FK_noticias_categoria_id` (`categoria_id`),
  CONSTRAINT `FK_noticias_categoria_id` FOREIGN KEY (`categoria_id`) REFERENCES `cat_noticias` (`categoria_id`),
  CONSTRAINT `noticias_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `cat_noticias` (`categoria_id`),
  CONSTRAINT `noticias_ibfk_2` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (1,'1917','La película ocurre en un escenario cierto: la I Guerra Mundial, que transcurrió entre 1914 y 1918 y que tuvo como eje central al continente europeo, especialmente la región del norte de Francia. En el conflicto perdieron la vida unos 17 millones de personas.',_binary '','2013-01-11','http://es.web.img3.acsta.net/c_215_290/pictures/20/01/09/15/10/0234685.jpg',1,3),(2,'Gran Torino','Cuatro años después de haber protagonizado la tercera obra maestra de su carrera, «Million Dollar Baby», Clint Eastwood volvió a ponerse delante de las cámaras para sacar adelante una película en teoría pequeña, de escaso presupuesto y que, sin embargo, se convertiría en la cuarta joya de su excelsa corona. Desde el primer fotograma «Gran Torino» deja estupefacto al espectador, al cinéfilo, al aficionado al cine de todo tipo, clase o estrato social. Sin alardes, casi en casa, rodando con su entrañable intimismo habitual, Eastwood forja una película de un pulso firme, una narrativa poderosa y un latido tan fuerte que te deja sin aliento.',_binary '','2012-11-27','https://www.artmajeur.com/medias/thumb/f/a/fasquelolivier/artwork/11789156_gran-torino.jpg',3,10),(3,'Pulp Fiction','Las peluquerías no se encuentran dentro de los rubros exceptuados en la cuarentena y los argentinos deben seguir recurriendo a instructivos de YouTube, consejos de expertos y métodos caseros para mantener el look. Recientemente, Flor Vigna quiso probar sus aptitudes con las tijeras y, según sus propias palabras, quedó demostrado que no es una de sus especialidades.',_binary '','2020-02-03','https://www.tonica.la/__export/1547169513722/sites/debate/img/2019/01/10/pulp_fiction_miramax_2.jpg_423682103.jpg',1,10),(4,'Batman y Robin','Si echamos un ojo al extenso currículum del guionista Akiva Goldsman, encontramos títulos tan interesantes como la serie Fringe o la película Una mente maravillosa, con la que incluso ganó el Oscar a Mejor guion adaptado en 2002.',_binary '','2020-05-23','https://e00-marca.uecdn.es/assets/multimedia/imagenes/2017/06/13/14973514665567.jpg',5,10),(5,'Sonic','La película de Sonic ha resultado en un auténtico bombazo de crítica y taquilla, con lo que ahora Sega se está abriendo a \"nuevas oportunidades\" para la marca de su erizo azul.',_binary '','2020-03-06','https://images-na.ssl-images-amazon.com/images/I/91wEJ0Y4odL.png',5,10),(6,'Harley Queen','Harley Quinn ha estado ganando popularidad gracias a su película y a la serie animada de DC Universe, pero al mismo tiempo se ha estado distanciando del Joker. En su lugar, el Príncipe Payaso del Crimen ha elegido a Punchline como nueva compañera que ha estado demostrando que es aún más letal que Harley.',_binary '','2020-05-06','https://dam.smashmexico.com.mx/wp-content/uploads/2018/04/24155045/Una-pelicula-con-Harley-Quinn-ya-tendria-directora-770x424.jpg',2,10),(7,'El Irlandes','El guión es excelente. Irreverente, mordaz, irónico, lleno de guiños. Las conversaciones entre el británico y el norteamericano son dinámicas siempre y van destapando las diferencias que hacen crecer a los dos personajes. Son el núcleo que soporta el entramado narrativo. Porque, aunque la trama principal es policial, todo queda en segundo plano para que conozcamos al personaje, su relación con el mundo y, por tanto, la confrontación entre culturas. El director maneja una subtrama en la que el policía y su madre son los protagonistas que no termina de funcionar bien (todo hay que decirlo). Posiblemente, pensó en buscar una arista al personaje que no termina de encontrar. Tal vez no existe.',_binary '','2013-01-11','https://media.metrolatam.com/2019/05/30/theirishman-647fe86ce24d693a88f476dc2e22e03b-1200x600.jpg',1,3),(8,'Parásitos','‘Parásitos’ es una de esas películas que no deja indiferente de principio a fin. Tarda poco en engancharte y no te suelta hasta un final arrollador. Con tal derroche de emociones, en un thriller tan cargado y rápido que no se detiene ni un momento, es normal que al acabar queramos recapitular un poco para entender todo lo que nos ha dicho Bong Joon-ho.La película cuenta la historia de una familia de cuatro miembros que se infiltra, a base de mentiras, como empleados domésticos de otra. Digamos que la película dirige nuestra atención e interés a cómo lo logran, a cómo mantienen esa farsa, antes de que todo estalle al final. Sin embargo, desde su propio título, ‘Parásitos’ juega con un doble sentido del término parasitario. La película comienza con un travelling en descenso que se repetirá también como último plano de la película. En él, vemos la venta a ras de calle de la casa subterránea en la que viven los protagonistas. Al empezar tienen dos problemas. El primero es el de eliminar la plaga de bichos que tienen en casa, por lo que el padre de familia decide dejar la ventana abierta para que entre el gas fumigador con ellos dentro. En el otro, vemos a los dos hijos buscando robarle el wifi a alguien. Ya sea un problema tecnológico o natural, nuestros protagonistas son parásitos, tanto en el sentido literal como figurado de la palabra. Pero el uso del término que da nombre al título del largometraje tiene un sentido mucho más complejo.',_binary '','2012-11-27','https://pics.filmaffinity.com/gisaengchung-379211692-mmed.jpg',4,10),(9,'Joker','Joker esta basado en el personaje del mismo nombre en DC Comics. Proporciona una historia original que explora por qué y cómo ha convertido al personaje infame en su ser actual.',_binary '','2020-02-03','https://ichef.bbci.co.uk/news/660/cpsprodpb/084C/production/_109142120_mediaitem109142119.jpg',1,10),(10,'Telnet','l director Christopher Nolan se ha ganado cierta fama por preferir los efectos especiales prácticos por encima de los generados por ordenador en sus películas. Quizás por esto no sea una sorpresa saber que para el rodaje de Tenet prefirió comprar y volar por los aires un avión 747 antes que usar miniaturas y efectos especiales.',_binary '','2020-05-07','http://es.web.img2.acsta.net/c_215_290/pictures/20/01/08/12/06/2814322.jpg',5,10),(11,'Avatar 2','A pesar de todo lo que está ocurriendo en el mundo, James Cameron sigue empeñado en hacer cuatro entregas más de \'Avatar\', aquella película que estrenó en 2009. Aunque el rodaje fue paralizado, claro, por el coronavirus, ya se ha reanudado y el director está seguro de que la fecha de estreno de \'Avatar 2\' seguiría siendo el 17 de diciembre de 2021. En las últimas semanas algunas fotos nos han mostrado a Cameron dirigiendo bajo el agua y a Sigourney Weaver y Kate Winslet en el rodaje. Y ahora tenemos unos nuevos detalles de la propia historia de la saga en esta nueva etapa.',_binary '','2020-03-06','https://img.ecartelera.com/noticias/fotos/61100/61131/1.jpg',5,10),(13,'zds','as',_binary '\0','2020-05-19','ae',1,3),(14,'Marco','as',_binary '\0','2020-05-12','as',1,3);
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personas` (
  `IdPersona` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(75) NOT NULL,
  `Apellido1` varchar(75) NOT NULL,
  `Apellido2` varchar(75) DEFAULT NULL,
  `FechaNacimiento` datetime NOT NULL,
  PRIMARY KEY (`IdPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (3,'Profesora','profe','profe','2018-02-10 00:00:00'),(4,'Antonio','García','Rodríguez','2018-01-27 00:03:33'),(5,'Gabriel','García','Pérez','2018-01-31 17:34:31'),(12,'Jorge','Mateos','García','1997-03-25 00:00:00'),(13,'edu','Eduardo','Juarez','1997-05-05 00:00:00'),(15,'Pedro','Sánchez','Pérez','1997-05-12 00:00:00'),(16,'Mariano','Rajoy','Brey','1997-05-19 00:00:00');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `IdProducto` int NOT NULL AUTO_INCREMENT,
  `URL` text NOT NULL,
  `Artista` text NOT NULL,
  `Genero` text NOT NULL,
  `Titulo` text NOT NULL,
  `Alquiler` tinyint NOT NULL DEFAULT '0',
  `PrecioCompra` int NOT NULL DEFAULT '0',
  `PrecioAlquiler` int NOT NULL DEFAULT '0',
  `FechaAlquiler` int NOT NULL DEFAULT '0',
  `FK_TipoProducto` int NOT NULL DEFAULT '3',
  PRIMARY KEY (`IdProducto`),
  KEY `FK_TipoProducto` (`FK_TipoProducto`),
  CONSTRAINT `FK_TipoProducto` FOREIGN KEY (`FK_TipoProducto`) REFERENCES `tipoproducto` (`IdTipoProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=1512122421 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (128497079,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/85/70/63/85706323-2b1b-6017-1cc5-3cb0608ca5c6/mza_7076314109117586593.jpg/200x200bb.png','Cadena SER','Noticias deportivas','El Larguero',1,10,10,7,3),(330326320,'https://is4-ssl.mzstatic.com/image/thumb/Podcasts123/v4/cd/ca/f1/cdcaf111-c453-e7af-4397-a23113682d48/mza_7583714768883418655.jpg/200x200bb.png','esRadio','Política','Es la Mañana de Federico',1,5,5,7,3),(330689086,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/6f/7b/80/6f7b805c-3304-c3c5-d0aa-40bf5898569f/mza_510057474165579130.jpg/200x200bb.png','esRadio','Política','En Casa de Herrero',0,5,0,0,3),(330984064,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts123/v4/37/ef/47/37ef471a-35b1-dd83-dda9-a323c4400cd7/mza_7208909536724458972.png/200x200bb.png','RAC1','Comedia','La competència - Programa sencer',0,5,0,0,3),(356329864,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/e7/34/78/e73478dd-c2a2-f4be-e18a-dd1eab955438/mza_2111265443719372257.jpg/200x200bb.png','Cadena SER','Historia','SER Historia',0,10,0,0,3),(391465654,'https://is2-ssl.mzstatic.com/image/thumb/Video123/v4/5d/97/40/5d974096-f695-a7e4-0b7a-7c06d139c307/pr_source.jpg/200x200bb.png','Tony Scott','Acción y aventura','Top Gun - Ídolos del aire',0,10,0,0,2),(397894942,'https://is4-ssl.mzstatic.com/image/thumb/Video/3a/3f/cd/mzi.ydwyftgi.jpg/200x200bb.png','Jonathan Mostow','Ciencia ficción y fantasía','Los Sustitutos (Surrogates)',1,5,5,7,2),(434035737,'https://is3-ssl.mzstatic.com/image/thumb/Video/09/cc/6a/mzi.ppuegtgk.jpg/200x200bb.png','Antoine Fuqua','Acción y aventura','El rey Arturo',1,10,5,7,2),(445843936,'https://is4-ssl.mzstatic.com/image/thumb/Video115/v4/20/2b/28/202b28f8-5e9c-bc30-ac81-f0196fcb8378/pr_source.lsr/200x200bb.png','Len Wiseman','Acción y aventura','La Jungla 4.0',0,20,0,0,2),(446199885,'https://is1-ssl.mzstatic.com/image/thumb/Video125/v4/cd/77/81/cd778162-bfd4-99be-4f31-2ea87ab98c1d/pr_source.lsr/200x200bb.png','John McTiernan','Ciencia ficción y fantasía','Depredador',1,15,10,7,2),(453434418,'https://is1-ssl.mzstatic.com/image/thumb/Video/4d/d5/fb/mzi.moxvbtny.jpg/200x200bb.png','John McTiernan','Acción y aventura','El Guerrero Nº13',0,5,0,0,2),(466208355,'https://is5-ssl.mzstatic.com/image/thumb/Video/2b/14/60/mzi.ijwryhdq.jpg/200x200bb.png','Michael Bay','Acción y aventura','La Roca',0,5,0,0,2),(467635868,'https://is2-ssl.mzstatic.com/image/thumb/Video/3e/d3/4f/mzi.gkeionyv.jpg/200x200bb.png','Tony Scott','Acción y aventura','Marea Roja (Crimson Tide)',0,10,0,0,2),(468636339,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/d5/ec/bf/d5ecbfc1-7237-cd1d-d574-07569588ba35/mza_17656696168174832716.jpg/200x200bb.png','OndaCero','Comedia','La Parroquia del Monaguillo',1,20,15,7,3),(468637823,'https://is2-ssl.mzstatic.com/image/thumb/Podcasts123/v4/f2/ca/c5/f2cac53b-012d-faa4-89a3-08940f851a78/mza_4667536150939257295.jpg/200x200bb.png','OndaCero','Cultura y sociedad','La rosa de los vientos',0,25,0,0,3),(499579910,'https://is4-ssl.mzstatic.com/image/thumb/Video/c1/dc/f3/mzl.vqwpcrli.jpg/200x200bb.png','Steven Soderbergh','Suspense','Contagio',1,25,15,7,2),(534942113,'https://is5-ssl.mzstatic.com/image/thumb/Video/v4/be/e7/d9/bee7d91c-424e-8a49-0a06-ce6a1a63b8d4/posterart_ES.jpg/200x200bb.png','Peter Weir','Acción y aventura','Master and Commander. Al otro lado del mundo',0,10,0,0,2),(568429405,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/9e/02/e2/9e02e249-3a30-ea41-6c0b-7a898a4010ae/mza_5954633546220156054.jpg/200x200bb.png','EuropaFM','Cultura y sociedad','Levántate y Cárdenas',0,30,0,0,3),(597132157,'https://is2-ssl.mzstatic.com/image/thumb/Video/v4/b8/1f/37/b81f372f-a315-0c78-7e91-b6190cbd6fa9/09107_MLPE_ES_IngloriousBasterds_1400x2100.jpg/200x200bb.png','Quentin Tarantino','Acción y aventura','Malditos bastardos',1,50,25,15,2),(601407334,'https://is1-ssl.mzstatic.com/image/thumb/Video/v4/e8/bf/12/e8bf1273-f084-4eb0-944b-e049d9de5307/DIEH01ES2521DB_DMD_Die_Hard_3.jpg/200x200bb.png','John McTiernan','Acción y aventura','Jungla de Cristal la Venganza',1,40,20,15,2),(647849954,'https://is4-ssl.mzstatic.com/image/thumb/Podcasts113/v4/3a/39/de/3a39de0e-1f84-61a1-9fcb-aade0110bc07/mza_6148410638717535414.jpg/200x200bb.png','Podium Podcast','Historia','La escóbula de la brújula',1,30,15,7,3),(665143133,'https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/10/ff/29/10ff29b7-a251-413c-8858-03d93778d86b/mza_6109589029350869812.jpg/200x200bb.png','Cadena SER','Comedia','Nadie Sabe Nada',0,30,0,0,3),(983656582,'https://is2-ssl.mzstatic.com/image/thumb/Podcasts113/v4/51/d0/89/51d0895b-178c-20ca-51ec-8f81f5d0fdd9/mza_18107209319735786849.jpg/200x200bb.png','OndaCero','Noticias','Más de uno',0,5,0,0,3),(1023685549,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/4e/29/e8/4e29e8c5-2e67-125e-9dc2-109a48846b5c/mza_2089974223167916751.jpg/200x200bb.png','Cadena SER','Historia','Cualquier tiempo pasado fue anterior',0,15,0,0,3),(1035694177,'https://is4-ssl.mzstatic.com/image/thumb/Podcasts113/v4/c0/d9/95/c0d9954b-a157-fcf9-1275-4e7fb3a276dc/mza_1588551638525293928.png/200x200bb.png','Cadena COPE','Noticias','Herrera en COPE',1,35,20,15,3),(1087967019,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts123/v4/fd/1a/92/fd1a92d9-378e-f696-5307-6bd6efc28a4b/mza_11663046317996835159.jpg/200x200bb.png','Fernando del Moral','Tecnología','La Manzana Mordida',0,10,0,0,3),(1146392357,'https://is4-ssl.mzstatic.com/image/thumb/Podcasts113/v4/7f/f8/ba/7ff8baa6-27fb-edef-e94d-256ec2c521f0/mza_12912145696106648139.png/200x200bb.png','Cadena COPE','Deportes','El Partidazo de COPE',1,30,15,7,3),(1278290988,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/a9/87/5c/a9875c5d-ea1c-316b-46fc-e8b4c5a805da/mza_1051576016635340176.jpg/200x200bb.png','Cadena SER','Comedia','La Vida Moderna',1,50,25,15,3),(1294524447,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/6c/ea/04/6cea049d-819f-1a1e-0366-494a0262801c/mza_12053734766852315793.jpg/200x200bb.png','Victor Abarca','Tecnología','Cafe con Victor',1,10,5,0,3),(1368017834,'https://is4-ssl.mzstatic.com/image/thumb/Podcasts114/v4/2d/a9/b3/2da9b3c6-1565-8cd9-fc91-13c08796ee17/mza_9420221542181058404.jpeg/200x200bb.png','TED','Cultura y sociedad','TED en Español',0,20,0,0,3),(1381331327,'https://is4-ssl.mzstatic.com/image/thumb/Podcasts113/v4/c3/21/2d/c3212d32-e7c8-e6dd-245a-4ece5a0f0cec/mza_3531789401769925480.jpg/200x200bb.png','César Vidal','Política','La Voz de César Vidal',1,20,10,7,3),(1381421296,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/53/00/3b/53003be7-a5e4-35fa-fa50-a498c8f84059/mza_8313098085136632844.jpg/200x200bb.png','Podium Podcast','Comedia','Aquí hay dragones',0,5,0,0,3),(1438309762,'https://is3-ssl.mzstatic.com/image/thumb/Video118/v4/ab/e5/2d/abe52dc9-30ab-26bc-012a-4ad291a68d18/mile_22_2000x3000_Itunes_CAS.jpg/200x200bb.png','Peter Berg','Acción y aventura','Milla 22',1,15,10,7,2),(1438698362,'https://is1-ssl.mzstatic.com/image/thumb/Video128/v4/76/71/0c/76710c20-09ae-dc41-96d4-e14605700edd/pr_source.lsr/200x200bb.png','Bryan Singer','Drama','Bohemian Rhapsody',1,50,25,15,2),(1455773624,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts123/v4/ef/07/09/ef0709d7-f410-8b74-381a-251a3da13d33/mza_9509533865956673281.jpg/200x200bb.png','Sergio Calderón y Carlos Ríos','Nutrición','Realfooding Life',0,10,0,0,3),(1460637754,'https://is5-ssl.mzstatic.com/image/thumb/Video113/v4/54/dd/7c/54dd7c89-520a-d223-3e1a-8da0b3e694d8/Alita_BattleAngel_2000x3000_ES.lsr/200x200bb.png','Robert Rodriguez','Acción y aventura','Alita: Ángel de Combate',1,10,5,7,2),(1464862624,'https://is1-ssl.mzstatic.com/image/thumb/Video123/v4/d4/2b/54/d42b54f2-ee7b-4329-7e6b-7bc172d62d7d/pr_source.lsr/200x200bb.png','Simon Kinberg','Acción y aventura','X-Men: Fénix Oscura',0,30,0,0,2),(1478761830,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/ca/d9/1e/cad91eb2-127d-cabc-8e1d-6bd569b3eb98/mza_504077614501836023.jpg/200x200bb.png','Applesfera','Tecnología','Loop Infinito (by Applesfera)',0,5,0,0,3),(1483566068,'https://is5-ssl.mzstatic.com/image/thumb/Video113/v4/f8/d6/cf/f8d6cffb-c374-cf99-f0fb-fee25e5a0439/pr_source.lsr/200x200bb.png','Tim Miller','Acción y aventura','Terminator: Destino oscuro',1,35,20,15,2),(1484741837,'https://is2-ssl.mzstatic.com/image/thumb/Video123/v4/d8/56/26/d8562682-b75e-1d92-6db5-da95fc0c2944/pr_source.lsr/200x200bb.png','James Mangold','Acción y aventura','Le Mans ´66',1,50,25,15,2),(1484769017,'https://is5-ssl.mzstatic.com/image/thumb/Video123/v4/e0/71/22/e071229c-a071-d54a-9bc7-7c8b5fb9cd85/pr_source.lsr/200x200bb.png','Edward Norton','Drama','Huérfanos de Brooklyn',0,20,0,0,2),(1484889413,'https://is4-ssl.mzstatic.com/image/thumb/Video113/v4/e4/32/0e/e4320e14-8caa-f6a2-de63-08a2571b7581/SPE_CHARLIES_ANGELS_2019_ES_ARTWORK_ES-ES_2000x3000_1ZNFYN00000HB9.lsr/200x200bb.png','Elizabeth Banks','Acción y aventura','Los Ángeles De Charlie',0,15,0,0,2),(1487022584,'https://is5-ssl.mzstatic.com/image/thumb/Video123/v4/b4/cd/66/b4cd66e1-5621-3eb3-682c-c8755ef30220/DIS_Frozen_II_TH_ES_2000X3000_WW_ARTWORK_EN_2000x3000_2276G4000000E0.lsr/200x200bb.png','Chris Buck & Jennifer Lee','Para toda la familia','Frozen II',1,40,20,15,2),(1487415381,'https://is1-ssl.mzstatic.com/image/thumb/Video113/v4/44/c5/cd/44c5cdb9-8059-33b4-ca3f-a386d38d4b1e/pr_source.lsr/200x200bb.png','Álvaro Fernandez-Armero','Comedia','Si yo fuera rico',1,40,20,15,2),(1488964698,'https://is4-ssl.mzstatic.com/image/thumb/Video114/v4/99/89/b4/9989b492-f873-700c-5aa7-8b37bae45770/SPE_JUMANJI_THE_NEXT_LEVEL_TH_FINAL_ES_ARTWORK_ES-ES_2000x3000_20O14H00000OFT.lsr/200x200bb.png','Jake Kasdan','Acción y aventura','Jumanji: Siguiente Nivel',1,30,15,7,2),(1489251504,'https://is4-ssl.mzstatic.com/image/thumb/Video123/v4/5f/10/35/5f10356e-5122-3504-2382-f890af7dfe0f/SPE_LITTLE_WOMEN_2019_TH_FINAL_ES_ARTWORK_ES-ES_2000x3000_209EWT00000KYZ.lsr/200x200bb.png','Greta Gerwig','Drama','Mujercitas',0,25,0,0,2),(1489425621,'https://is1-ssl.mzstatic.com/image/thumb/Video123/v4/cc/df/f3/ccdff314-9935-5713-76f2-92edce58f190/pr_source.lsr/200x200bb.png','Tom Hooper','Musicales','Cats',0,5,0,0,2),(1490521180,'https://is5-ssl.mzstatic.com/image/thumb/Video124/v4/de/7d/bf/de7dbf9b-77dc-e752-0cde-6ad560b7a238/DIS_SWRoS_TH_ES_OCAD_ES_ARTWORK_ES-ES_2000x3000_24C97R000000J6.lsr/200x200bb.png','J.J. Abrams','Acción y aventura','Star Wars: El ascenso de Skywalker',1,50,25,15,2),(1490719646,'https://is1-ssl.mzstatic.com/image/thumb/Video123/v4/db/7e/27/db7e27be-417f-f2b2-8545-d26c30a549e6/pr_source.lsr/200x200bb.png','Destin Daniel Cretton','Drama','Cuestión de justicia',1,45,20,15,2),(1491270441,'https://is3-ssl.mzstatic.com/image/thumb/Video124/v4/d3/63/a3/d363a32d-1ee9-f09d-d4dd-d0300498da3c/pr_source.lsr/200x200bb.png','Troy Quane & Nick Bruno','Para toda la familia','Espías con disfraz',1,20,10,7,2),(1491275317,'https://is4-ssl.mzstatic.com/image/thumb/Video113/v4/6c/9a/a1/6c9aa109-3040-5eac-99de-0bebed14d42b/pr_source.lsr/200x200bb.png','Robert Eggers','Drama','El faro (2019)',0,20,0,0,2),(1491842471,'https://is4-ssl.mzstatic.com/image/thumb/Video113/v4/08/8b/75/088b75a2-2bec-5103-ccc7-4f557c0a980e/SPE_THE_GRUDGE_2020_FINAL_ES_ARTWORK_ES-ES_2000x3000_2262X700000JI1.lsr/200x200bb.png','Nicolas Pesce','Terror','La Maldición',1,20,10,7,2),(1492469162,'https://is2-ssl.mzstatic.com/image/thumb/Video113/v4/a1/44/cb/a144cb6c-4c85-8509-2491-7623be294555/89X48_MLPE_artwork_PosterArt_en.lsr/200x200bb.png','Sam Mendes','Drama','1917',1,50,25,15,2),(1493611486,'https://is2-ssl.mzstatic.com/image/thumb/Video123/v4/01/a6/06/01a60676-6956-8c39-e0c3-30d7285e826f/WB_MALASANA_32_TH_WW_ARTWORK_EN_2000x3000_23B332000000DM.lsr/200x200bb.png','Albert Pintó','Terror','Malasaña 32',0,30,0,0,2),(1494048148,'https://is4-ssl.mzstatic.com/image/thumb/Video123/v4/3a/ea/6d/3aea6d14-6938-84b1-5e6e-a0cb843c5391/SPE_BAD_BOYS_FOR_LIFE_FINAL_ES_ARTWORK_EN_ES-ES_2000x3000_233N4W00000KNQ.lsr/200x200bb.png','Adil & Bilall','Acción y aventura','Bad Boys For Life',1,25,15,7,2),(1495244246,'https://is3-ssl.mzstatic.com/image/thumb/Video123/v4/01/34/73/013473b7-1b45-a500-d8e1-4a85ebc09d38/IT105780_artwork_WW.jpg/200x200bb.png','Bong Joon Ho','Comedia','Parásitos',1,50,25,15,2),(1496058187,'https://is3-ssl.mzstatic.com/image/thumb/Video113/v4/fb/9c/47/fb9c4742-6a36-d9cc-a384-660609afd3d1/pr_source.lsr/200x200bb.png','Jeff Fowler','Para toda la familia','Sonic la película',0,20,0,0,2),(1496062625,'https://is5-ssl.mzstatic.com/image/thumb/Video113/v4/3c/9c/b5/3c9cb5d7-3382-fba4-45f7-e81e7b445707/pr_source.lsr/200x200bb.png','Cathy Yan','Acción y aventura','Aves de presa (y la fantabulosa emancipación de Harley Quinn)',1,20,10,7,2),(1497418487,'https://is5-ssl.mzstatic.com/image/thumb/Video123/v4/40/bd/d7/40bdd734-2808-a3f7-a7f3-aa0b8545c005/Midway_2000x3000_ES_iTunes.jpg/200x200bb.png','Roland Emmerich','Acción y aventura','Midway',0,10,0,0,2),(1498686243,'https://is3-ssl.mzstatic.com/image/thumb/Video124/v4/97/74/46/977446b9-d3f8-349a-0350-8e28ce3d904b/IT105802_artwork_WW.jpg/200x200bb.png','Philippe de Chauveron','Comedia','Dios mío, ¿Pero qué te hemos hecho... ahora?',0,10,0,0,2),(1499927976,'https://is3-ssl.mzstatic.com/image/thumb/Video114/v4/cb/37/2a/cb372a5c-3e0c-7cea-56cd-4c97e9a5084f/SPE_BLOODSHOT_TH_PREORDER_ES_ARTWORK_EN_2000x3000_259FM5000003QD.jpg/200x200bb.png','David S. F. Wilson','Acción y aventura','Bloodshot',0,5,0,0,2),(1500104125,'https://is1-ssl.mzstatic.com/image/thumb/Video124/v4/8c/c3/82/8cc382bd-06a9-79fe-2d82-f50c50df9cc8/DIS_ONWARD_R1_WW_ARTWORK_EN_2000x3000_2598AP000000IQ.lsr/200x200bb.png','Dan Scanlon','Para toda la familia','Onward',0,5,0,0,2),(1500958564,'https://is2-ssl.mzstatic.com/image/thumb/Video114/v4/d3/0a/9b/d30a9b7f-8e73-349e-b82c-91e402f2c1a0/IT105845_artwork_WW.jpg/200x200bb.png','Roman Polanski','Drama','El Oficial y el Espía',0,5,0,0,2),(1501430234,'https://is1-ssl.mzstatic.com/image/thumb/Video114/v4/81/8d/29/818d2988-5093-fa7f-048f-5e7bd0f60292/89X62_MLPE_artwork_PosterArt_es-ES.jpg/200x200bb.png','Rian Johnson','Comedia','Puñales por la espalda',1,10,5,7,2),(1501570094,'https://is5-ssl.mzstatic.com/image/thumb/Music114/v4/76/6f/b3/766fb302-4ca8-63ce-e0af-cb01ff6a76ef/190295254339.jpg/200x200bb.png','Lianne La Havas','R&B/Soul','Paper Thin',1,10,5,7,1),(1502254973,'https://is5-ssl.mzstatic.com/image/thumb/Music113/v4/f7/c5/f5/f7c5f5e2-b5b9-92a0-8707-a1eda4fe7b16/5400863030117_cover.jpg/200x200bb.png','Arlo Parks','R&B/Soul','Black Dog',0,5,0,0,1),(1502780889,'https://is2-ssl.mzstatic.com/image/thumb/Video113/v4/99/1e/eb/991eeb4e-4c3e-a61a-bfd7-8276bbcd7bc6/IT105843_artwork_WW.jpg/200x200bb.png','Brian De Palma','Suspense','Domino (de Brian De Palma)',0,10,0,0,2),(1503412937,'https://is2-ssl.mzstatic.com/image/thumb/Video113/v4/8a/4d/ca/8a4dca5c-0fb8-5526-b8fd-7b52f45b3573/IT105862_artwork_WW.jpg/200x200bb.png','Alfonso Gomez-Rejon','Drama','La Guerra de las Corrientes',0,5,0,0,2),(1503749185,'https://is4-ssl.mzstatic.com/image/thumb/Video123/v4/78/e1/e2/78e1e26b-4e50-e0f9-6f99-c07c248a7181/BCF410N0_ElMisterioDelDragon_WW_PosterArt_2000x3000.jpg/200x200bb.png','Oleg Stepchenko','Acción y aventura','El Misterio del Dragón',1,50,25,15,2),(1503750209,'https://is1-ssl.mzstatic.com/image/thumb/Podcasts123/v4/08/a6/bf/08a6bf62-5de0-f855-b6c8-49a2449c4700/mza_5728230689228876820.jpg/200x200bb.png','CARLA ZAPLANA','Nutrición','CARLA ZAPLANA : ORIGENES',1,10,5,7,3),(1505850533,'https://is5-ssl.mzstatic.com/image/thumb/Video113/v4/88/f4/14/88f41435-d2fb-2857-3ae9-515861883eec/IT105941_artwork_WW.jpg/200x200bb.png','Guy Nattiv','Drama','Skin (2018)',0,20,0,0,2),(1506057882,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/32/38/fc/3238fce3-b9dc-8015-a51d-29ed056b9d00/190295232283.jpg/200x200bb.png','L Devine','Pop','Don\'t Say It',1,15,10,7,1),(1506568617,'https://is1-ssl.mzstatic.com/image/thumb/Music113/v4/a2/c2/cf/a2c2cf94-961b-5efc-4a96-195cdb89eabb/8445162068383.jpg/200x200bb.png','Isius','Pop','On Your Bike',0,30,0,0,1),(1506657004,'https://is2-ssl.mzstatic.com/image/thumb/Video123/v4/6c/b4/68/6cb4680a-ee80-cd82-a2b1-88f40c156e4d/IT106019_artwork_WW.jpg/200x200bb.png','Dani de la Orden','Comedia','Hasta que la boda nos separe',1,20,10,7,2),(1508356753,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/42/4f/da/424fda5d-b3e2-d153-c4db-8f9bc43d7e83/886448367468.jpg/200x200bb.png','Natalia Lafourcade & Carlos Rivera','Música mexicana','Mexicana Hermosa',0,10,0,0,1),(1508422270,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/93/9d/25/939d25f0-0428-3381-2170-06fbe2d8b531/20UMGIM27191.rgb.jpg/200x200bb.png','JPEGMAFIA & Denzel Curry','Hip-hop/Rap','BALD REMIX',1,10,5,7,1),(1509014151,'https://is3-ssl.mzstatic.com/image/thumb/Music113/v4/74/72/25/7472256d-bd22-f0f8-8da1-493dc2dc6dd5/887829112660.png/200x200bb.png','Dirty Projectors','Alternativa','Lose Your Love',0,5,0,0,1),(1509335875,'https://is2-ssl.mzstatic.com/image/thumb/Music123/v4/03/16/2a/03162ab0-f012-8cbf-74d0-a0fe9dacccc4/8445162081597.jpg/200x200bb.png','Cora Yako','Pop indie','A Flor de Piel',1,10,5,7,1),(1509337311,'https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/72/4a/b4/724ab4ef-5815-9de6-e970-3c1dc49c690d/886448353119.jpg/200x200bb.png','Vicentico','Alternativa y rock en español','No Tengo',0,5,0,0,1),(1509348668,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/cf/d5/51/cfd5518f-5cf0-e235-9b56-d4779d183bd5/0.jpg/200x200bb.png','Sean Paul','Pop','Back It up Deh',0,10,0,0,1),(1509561877,'https://is5-ssl.mzstatic.com/image/thumb/Music123/v4/0d/86/a1/0d86a1be-f8dc-9687-afb7-a7499e6add98/886448359166.jpg/200x200bb.png','Monsieur Periné','Música latina','Mundo Paralelo (feat. Pedro Capó)',0,20,0,0,1),(1509815350,'https://is3-ssl.mzstatic.com/image/thumb/Music113/v4/75/8e/79/758e7921-a0a8-01ea-3b9f-2c62a8f0c05b/20UMGIM20918.rgb.jpg/200x200bb.png','Lérica & Belinda','Pop en español','Flamenkito',0,5,0,0,1),(1509816775,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/55/26/39/552639df-e959-6c67-bdda-292f3644f743/20UMGIM26935.rgb.jpg/200x200bb.png','Miki Núñez','Pop en español','Me Vale',0,5,0,0,1),(1509821981,'https://is2-ssl.mzstatic.com/image/thumb/Music123/v4/c4/94/1a/c4941a3e-b711-d36f-5c33-1b9bb8800d99/20UMGIM31509.rgb.jpg/200x200bb.png','Donna Missal','Pop','Hurt By You (Stripped)',0,5,0,0,1),(1509831661,'https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/a1/2c/62/a12c6266-068d-dbb2-8cfb-678df96f4d6c/mza_7740786458073183388.jpg/200x200bb.png','Victor Abarca','Tecnología','Espresso con Victor',0,5,0,0,3),(1509848146,'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/c3/51/5d/c3515d89-8b70-4893-89ca-e1f71234e8ee/20UMGIM27441.rgb.jpg/200x200bb.png','JP Cooper','Pop','Little Bit of Love',0,5,0,0,1),(1509848677,'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/30/02/4a/30024a5e-b544-4415-3f70-a311eb3f86c0/20UMGIM18461.rgb.jpg/200x200bb.png','The Kooks','Rock','Tea And Biscuits',0,5,0,0,1),(1509849893,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/c6/c5/37/c6c5376a-4d4f-594a-42a8-ec671ac4c2a4/20UMGIM30732.rgb.jpg/200x200bb.png','Roo Panes','Cantautores','Listen To The One Who Loves You',0,5,0,0,1),(1509944075,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/71/b3/8a/71b38a77-956d-a4f2-e70c-43a15086a4f9/886448434788.jpg/200x200bb.png','Arde Bogotá','Pop','Quiero Casarme Contigo',1,10,5,7,1),(1509951632,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bf/fd/a3/bffda372-55ec-663f-a3b2-07cd7d76907e/190295221812.jpg/200x200bb.png','Sidecars','Pop','Mundo imperfecto',0,5,0,0,1),(1509951655,'https://is2-ssl.mzstatic.com/image/thumb/Music123/v4/6d/13/22/6d13229b-4392-48bb-26f9-664aee13acac/190295221515.jpg/200x200bb.png','Iza Bastet','Música latina','Solo Déjate Llevar',0,10,0,0,1),(1509964545,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/72/38/48/7238489c-d210-2fb7-4371-1dd21a27c773/190295221560.jpg/200x200bb.png','MARINA','Pop','No Debería Quererte',0,5,0,0,1),(1509982633,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/20/44/cb/2044cb3e-f114-cb72-e6f7-f20b512cebd2/20UMGIM32084.rgb.jpg/200x200bb.png','Blossoms','Alternativa','Lost (In Isolation)',0,5,0,0,1),(1510014942,'https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/39/ee/bd/39eebda3-8cb8-5449-94c2-3f36ce76211b/886448400837.jpg/200x200bb.png','Arizona Zervas','Hip-hop/Rap','24',0,5,0,0,1),(1510020820,'https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/98/60/75/9860754f-157d-1d03-2326-1ef9a7777710/190295220938.jpg/200x200bb.png','Jacobo Serra','Pop','A plena luz (con Xoel López) [with Xoel López]',0,5,0,0,1),(1510066669,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/cf/2a/9d/cf2a9dd0-d0ff-b104-ec01-8b75ee3d57ba/8445162087674.jpg/200x200bb.png','Mario Díaz','Salsa y Tropical','Cambios (feat. Colectivo Panamera)',0,5,0,0,1),(1510331181,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/c2/1b/ca/c21bca20-9bcd-2c6e-d9c1-d5b5edd0e0cd/8445162092579.jpg/200x200bb.png','nostalgia.en.los.autobuses','Folk','delfines (feat. Raül Refree)',0,5,0,0,1),(1510354071,'https://is5-ssl.mzstatic.com/image/thumb/Music123/v4/dd/0c/70/dd0c70a6-8889-72f7-e6b2-1440aa2388d5/886448448549.jpg/200x200bb.png','Manolo García','Pop en español','Somos Levedad (Acústico)',0,5,0,0,1),(1510473963,'https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/45/f7/62/45f76234-db15-9955-f3bd-6493314f64f0/886448443353.jpg/200x200bb.png','Yebba','Pop','Distance',0,5,0,0,1),(1510487348,'https://is5-ssl.mzstatic.com/image/thumb/Music123/v4/1c/b1/c1/1cb1c1bd-da40-c69a-db75-5b4859b28b71/8445162092692.jpg/200x200bb.png','IZARO','Pop','Tiempo Ausente',0,5,0,0,1),(1510554442,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/eb/c5/69/ebc5698b-e261-da54-d5b2-0362b84c0ee2/886448437680.jpg/200x200bb.png','Kimberley Tell','Pop en español','Gastando Horas',0,5,0,0,1),(1510554804,'https://is2-ssl.mzstatic.com/image/thumb/Music123/v4/ce/d1/1e/ced11ee8-8920-0a23-04ab-da644dbc3d71/886448452478.jpg/200x200bb.png','C. Tangana','Pop','Guille Asesino',1,20,10,7,1),(1510568898,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/9f/09/08/9f0908f9-f9d5-1e0d-b751-586d426512c0/8425845931791.jpg/200x200bb.png','Flashy Ice Cream','Urbano latino','ALBA',0,5,0,0,1),(1510569799,'https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/08/44/e2/0844e270-8a1e-4f91-e5b4-36eb871bdde5/8445162093446.jpg/200x200bb.png','Belén Aguilera','Pop','Tu Piel',0,5,0,0,1),(1510594820,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/8b/c9/c2/8bc9c2d9-2ffd-b511-a806-f0f155fc6bcb/886448442394.jpg/200x200bb.png','Lil Tjay','Hip-hop/Rap','Ice Cold',0,20,0,0,1),(1510624472,'https://is2-ssl.mzstatic.com/image/thumb/Music123/v4/0f/19/d2/0f19d26c-b9c3-20ca-7078-0c962f8a14cc/886448450634.jpg/200x200bb.png','Ray LaMontagne','Rock','We\'ll Make It Through',0,15,0,0,1),(1510625067,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/13/1f/6e/131f6eec-38ea-2f86-e405-828dfb061b54/886448419785.jpg/200x200bb.png','Audrey Mika','Pop','Just Friends',0,5,0,0,1),(1510639904,'https://is1-ssl.mzstatic.com/image/thumb/Music113/v4/88/7b/d6/887bd63d-f3a8-d847-54a3-5818eb4c34fe/075679821706.jpg/200x200bb.png','Kehlani','R&B/Soul','Can I (feat. Tory Lanez)',0,5,0,0,1),(1510747093,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/92/17/a0/9217a0fc-bbdd-3315-42f6-336cfc0581be/886448455837.jpg/200x200bb.png','Ana Mena & DELLAFUENTE','Urbano latino','La Pared',0,5,0,0,1),(1510840040,'https://is5-ssl.mzstatic.com/image/thumb/Music113/v4/ad/fb/39/adfb399c-7b4f-6b85-5f25-c85408277351/20UMGIM34012.rgb.jpg/200x200bb.png','Hailee Steinfeld','Pop','End this (L.O.V.E.)',0,5,0,0,1),(1510851924,'https://is5-ssl.mzstatic.com/image/thumb/Music123/v4/fb/eb/43/fbeb4331-beab-a84f-ac38-eb9f20d6f01b/886448422792.jpg/200x200bb.png','A.CHAL','R&B/Soul','Hollywood Love (feat. Gunna)',0,5,0,0,1),(1510875619,'https://is5-ssl.mzstatic.com/image/thumb/Music123/v4/2a/f2/fe/2af2fe8c-39dd-a4e6-309d-cd0d9d6c4e0f/BadBoys3MovieCollection_2000x3000_LSR_ES.lsr/200x200bb.png','Sony Pictures Entertainment','Acción y aventura','Pack Bad Boys 1-3',0,5,0,0,2),(1510885042,'https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/b0/96/87/b09687cf-535e-1e7a-71a2-69ef60f67b44/Jumanji3FilmCollection_2000x3000_LSR_ES.lsr/200x200bb.png','Sony Pictures Entertainment','Acción y aventura','Jumanji - Colección 3 pelícuååas',1,50,25,15,2),(1510936037,'https://is3-ssl.mzstatic.com/image/thumb/Podcasts113/v4/3e/82/20/3e822052-6ff8-ea09-d8a8-6d343701ff68/mza_18001233191048053924.jpg/200x200bb.png','KIKILLO','Historia','NO es una chapa cualquiera',0,5,0,0,3),(1510963896,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/10/ad/70/10ad709c-190d-ff77-b12b-d97fd16175a1/886448459583.jpg/200x200bb.png','Vetusta Morla','Pop','Los Abrazos Prohibidos',0,15,0,0,1),(1510973720,'https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/5e/9a/4c/5e9a4c00-6b9a-36be-b92f-37440700591a/20UMGIM31285.rgb.jpg/200x200bb.png','Sen Senra','Pop en español','Nada Y Nadie',0,5,0,0,1),(1511029016,'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/df/0e/48/df0e4828-9958-0b93-9c07-f43fe27f0c42/075679827814.jpg/200x200bb.png','Hayley Williams','Alternativa','Pure Love',0,5,0,0,1),(1511203453,'https://is5-ssl.mzstatic.com/image/thumb/Music113/v4/50/f8/52/50f852aa-f857-ea33-8327-f9d21855817b/190295215033.jpg/200x200bb.png','Gorillaz','Alternativa','How Far? (feat. Tony Allen and Skepta)',0,5,0,0,1),(1511235573,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/48/e6/3f/48e63f46-ae93-7933-37e8-000194705849/075679814746.jpg/200x200bb.png','Sia','Pop','Saved My Life',0,5,0,0,1),(1511522410,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/7e/ca/92/7eca92b9-8814-18da-1182-a3e05070e5ba/190295214685.jpg/200x200bb.png','Jorge Roman','Pop','Te Pido Perdón',0,5,0,0,1),(1511562950,'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/cb/42/e6/cb42e61e-0f29-4724-d9b9-be443466b4b5/20UMGIM34924.rgb.jpg/200x200bb.png','Ariana Grande & Justin Bieber','Pop','Stuck with U',0,5,0,0,1),(1511586138,'https://is3-ssl.mzstatic.com/image/thumb/Music113/v4/60/e1/02/60e10267-3113-872e-f77a-930cd18bf1db/054391935038.jpg/200x200bb.png','Cher','Pop','Chiquitita (Spanish Version)',0,5,0,0,1),(1511650858,'https://is1-ssl.mzstatic.com/image/thumb/Music123/v4/ad/69/0d/ad690d96-84b8-babe-2da3-e2aa4a373ec1/886448466192.jpg/200x200bb.png','Chris Brown & Young Thug','R&B/Soul','Go Crazy',0,5,0,0,1),(1512122420,'https://is3-ssl.mzstatic.com/image/thumb/Music113/v4/f1/68/2a/f1682aa6-84e7-7298-5321-87e6c981ac90/190295215613.jpg/200x200bb.png','Charli XCX','Pop','i finally understand',0,10,0,0,1);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `IdRol` int NOT NULL AUTO_INCREMENT,
  `TipoUsuario` char(1) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`IdRol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'U','Usuario'),(2,'T','Trabajador'),(3,'A','Administrador');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoproducto`
--

DROP TABLE IF EXISTS `tipoproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoproducto` (
  `IdTipoProducto` int NOT NULL AUTO_INCREMENT,
  `TipoProducto` char(1) NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdTipoProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoproducto`
--

LOCK TABLES `tipoproducto` WRITE;
/*!40000 ALTER TABLE `tipoproducto` DISABLE KEYS */;
INSERT INTO `tipoproducto` VALUES (1,'M','Música'),(2,'P','Película'),(3,'T','Podcast');
/*!40000 ALTER TABLE `tipoproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `IdUsuario` int NOT NULL AUTO_INCREMENT,
  `User` varchar(20) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Estado` tinyint NOT NULL DEFAULT '0',
  `IdPersona` int NOT NULL,
  `IdRol` int NOT NULL,
  `ContadorInicioSesion` int NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '100',
  PRIMARY KEY (`IdUsuario`),
  UNIQUE KEY `IdUsuario_UNIQUE` (`IdUsuario`),
  KEY `FK_Usuario_Persona` (`IdPersona`),
  KEY `FK_Usuario_Rol` (`IdRol`),
  CONSTRAINT `FK_Usuario_Persona` FOREIGN KEY (`IdPersona`) REFERENCES `personas` (`IdPersona`),
  CONSTRAINT `FK_Usuario_Rol` FOREIGN KEY (`IdRol`) REFERENCES `roles` (`IdRol`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Profesora','Profe',0,3,1,0,180),(3,'Marco','Marco',0,4,3,0,160),(10,'jorgmateos','admin',0,12,1,0,540),(11,'edu','a',0,13,1,0,100),(12,'pedrito','psoe',0,15,1,0,100),(13,'mariano','pp',0,16,1,0,100);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'TheContentBox'
--

--
-- Dumping routines for database 'TheContentBox'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-31 22:22:58
