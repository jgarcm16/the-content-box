/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jorgmateos
 */
@Entity
@Table(name="mensajes")
public class Mensaje implements Serializable{
    
    // Constructor vacío
    // --------------  //
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idMensaje;
    
    @Column(name="ContMensaje")
    private String contenido;
    
    @Column(name="Estado")
    private boolean estado;
    
    @Column(name="Asunto")
    private String asunto;
    
    @Column(name="Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    
    // FOREING KEYS
    @JoinColumn(name="IdUsuaOrigen")
    private Usuario emisor;
    
    @JoinColumn(name="IdUsuaDestino")
    private Usuario receptor;
    
    // Getters & setters
    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public Usuario getEmisor() {
        return emisor;
    }

    public void setEmisor(Usuario emisor) {
        this.emisor = emisor;
    }

    public Usuario getReceptor() {
        return receptor;
    }

    public void setReceptor(Usuario receptor) {
        this.receptor = receptor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + this.idMensaje;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mensaje other = (Mensaje) obj;
        if (this.idMensaje != other.idMensaje) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mensaje{" + "idMensaje=" + idMensaje + ", contenido=" + contenido + ", estado=" + estado + ", asunto=" + asunto + ", emisor=" + emisor + ", receptor=" + receptor + '}';
    }
    
    
    

    
}
