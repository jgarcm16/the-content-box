/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jorgmateos
 */
@Entity
@Table(name="TipoProducto")
public class TipoProducto implements Serializable{
    
    // Constructor vacío
    // --------------  //
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idTipoProducto;
    
    @Column(name="TipoProducto")
    private char tipoProductoChar;
    
    @Column(name="Descripcion")
    private String descripcion;
    
    // Getters & setters
    public int getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(int idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public char getTipoProductoChar() {
        return tipoProductoChar;
    }

    public void setTipoProductoChar(char tipoProductoChar) {
        this.tipoProductoChar = tipoProductoChar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.idTipoProducto;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoProducto other = (TipoProducto) obj;
        if (this.idTipoProducto != other.idTipoProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoProducto{" + "idTipoProducto=" + idTipoProducto + ", tipoProductoChar=" + tipoProductoChar + ", descripcion=" + descripcion + '}';
    }
    
    
    
    
}
