/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author jorgmateos
 */
@Entity
@Table(name="favoritos")
public class Favorito implements Serializable{
    
    // Constructor vacío
    // --------------  //
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idFavorito;
    
    @Column(name="Comentario")
    private String comentario;
    
    @Column(name="Valoracion")
    private int valoracion;
    
    // FOREING KEYS
    @JoinColumn(name="IdProducto")
    private Producto idProducto;
    
    @JoinColumn(name="IdUsuario")
    private Usuario idUsuario;
    
    // Getters & setters
    public int getIdFavorito() {
        return idFavorito;
    }

    public void setIdFavorito(int idFavorito) {
        this.idFavorito = idFavorito;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getValoracion() {
        return valoracion;
    }

    public void setValoracion(int valoracion) {
        this.valoracion = valoracion;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.idFavorito;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Favorito other = (Favorito) obj;
        if (this.idFavorito != other.idFavorito) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Favorito{" + "idFavorito=" + idFavorito + ", comentario=" + comentario + ", valoracion=" + valoracion + ", idProducto=" + idProducto + ", idUsuario=" + idUsuario + '}';
    }
    
    
    
    
}
