/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 *
 * @author jorgmateos
 */
@Entity
@Table(name="Productos")
public class Producto implements Serializable{
    
    // Constructor vacío
    // --------------  //
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idProducto;
    
    @Column(name="Titulo")
    private String titulo;
    
    @Column(name="Artista")
    private String artista;
    
    @Column(name="Genero")
    private String genero;
    
    @Column(name="URL")
    private String url;
    
    @Column(name="Alquiler")
    private boolean alquiler;
    
    @Column(name="PrecioCompra")
    private int precioCompra;
    
    @Column(name="PrecioAlquiler")
    private int precioAlquiler;
    
    @Column(name="FechaAlquiler")
    private int fechaAlquiler;
    
    // FOREING KEYS
    @JoinColumn(name="FK_TipoProducto")
    @ManyToOne(cascade=CascadeType.PERSIST)
    private TipoProducto idTipoProducto;
    
    // Getters & setters
    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isAlquiler() {
        return alquiler;
    }

    public void setAlquiler(boolean alquiler) {
        this.alquiler = alquiler;
    }

    public int getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(int precioCompra) {
        this.precioCompra = precioCompra;
    }

    public int getPrecioAlquiler() {
        return precioAlquiler;
    }

    public void setPrecioAlquiler(int precioAlquiler) {
        this.precioAlquiler = precioAlquiler;
    }

    public int getFechaAlquiler() {
        return fechaAlquiler;
    }

    public void setFechaAlquiler(int fechaAlquiler) {
        this.fechaAlquiler = fechaAlquiler;
    }

    public TipoProducto getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(TipoProducto idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (this.idProducto != other.idProducto) {
            return false;
        }
        
         if (this.titulo != other.titulo) {
            return false;
        }
         
         if (this.artista != other.artista) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Producto{" + "idProducto=" + idProducto + ", titulo=" + titulo + ", artista=" + artista + ", genero=" + genero + ", url=" + url + ", alquiler=" + alquiler + ", precioCompra=" + precioCompra + ", precioAlquiler=" + precioAlquiler + ", fechaAlquiler=" + fechaAlquiler + ", idTipoProducto=" + idTipoProducto + '}';
    }
    
    
    
    
    
}
