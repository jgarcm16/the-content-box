/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jorgmateos
 */

@Entity
@Table(name="Menus")
public class Menu implements Serializable {
    
    // Constructor vacío
    // --------------  //
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idMenu;
    
    @Column(name="Nombre")
    private String nombre;
    
    @Column(name="Tipo")
    @Enumerated(EnumType.STRING)
    private TipoMenu tipo;
    
    @Column(name="Estado")
    private boolean estado;
    
    @Column(name="URL")
    private String url;
    
     // FOREIGN KEYS
    @JoinColumn(name="IdRol")
    @ManyToOne(cascade=CascadeType.PERSIST) 
    private Rol idRol;
    
    @JoinColumn(name="IdPadre")
    @ManyToOne(cascade=CascadeType.PERSIST) 
    private Menu idPadre;
    
    
    // Getters & setters
    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoMenu getTipo() {
        return tipo;
    }

    public void setTipo(TipoMenu tipo) {
        this.tipo = tipo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }

    public Menu getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Menu idPadre) {
        this.idPadre = idPadre;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.idMenu;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Menu other = (Menu) obj;
        if (this.idMenu != other.idMenu) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Menu{" + "idMenu=" + idMenu + ", nombre=" + nombre + ", tipo=" + tipo + ", estado=" + estado + ", url=" + url + ", idRol=" + idRol + ", idPadre=" + idPadre + '}';
    }
    
    
    
    
            
    
    
}
