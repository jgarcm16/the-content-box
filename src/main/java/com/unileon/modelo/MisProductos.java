/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jorgmateos
 */
@Entity
@Table(name="misproductos")
public class MisProductos implements Serializable{
    
    // Constructor vacío
    //------------------//
    
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idMisProductos;
    
    @Column(name="FechaAdquisicion")
    @Temporal(TemporalType.DATE)
    private Date fechaAdquisicion;
    
    @Column(name="FechaExpiracion")
    @Temporal(TemporalType.DATE)
    private Date fechaExpiracion;
    
    @Column(name="Estado")
    private boolean estado;
    
    @Column(name="Alquilado")
    private boolean alquilado;
    
    @Column(name="DiasAlquiler")
    private int diasAlquiler;
    
    // FOREIGN KEYS
    @JoinColumn(name="IdProducto")
    private Producto idProducto;
    
    @JoinColumn(name="IdUsuario")
    private Usuario idUsuario;
    
    // Getters & setters
    public int getIdMisProductos() {
        return idMisProductos;
    }

    public void setIdMisProductos(int idMisProductos) {
        this.idMisProductos = idMisProductos;
    }

    public Date getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(Date fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isAlquilado() {
        return alquilado;
    }

    public void setAlquilado(boolean alquilado) {
        this.alquilado = alquilado;
    }

    public int getDiasAlquiler() {
        return diasAlquiler;
    }

    public void setDiasAlquiler(int diasAlquiler) {
        this.diasAlquiler = diasAlquiler;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.idMisProductos;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MisProductos other = (MisProductos) obj;
        if (this.idMisProductos != other.idMisProductos) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MisProductos{" + "idMisProductos=" + idMisProductos + ", fechaAdquisicion=" + fechaAdquisicion + ", estado=" + estado + ", alquilado=" + alquilado + ", diasAlquiler=" + diasAlquiler + ", idProducto=" + idProducto + ", idUsuario=" + idUsuario + '}';
    }
    
    
    

    
    
}
