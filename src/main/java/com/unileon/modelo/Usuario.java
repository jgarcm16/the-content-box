/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author eduardojuarezrobles
 */
@Entity
@Table(name="usuarios")
public class Usuario implements Serializable {
    
    // Constructor vacío
    //------------------//
    
    // Atributos privados
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idUsuario;
    
    @Column(name="User")
    private String username;
    
    @Column(name="Password")
    private String password;

    @Column(name="Estado")
    private boolean estado;
    
    @Column(name="ContadorInicioSesion")
    private int contadorInicioSesion;
    
    @Column(name="Saldo")
    private int saldo;
    
    // FOREIGN KEYS
    @JoinColumn(name="IdPersona")
    @OneToOne(cascade=CascadeType.PERSIST) 
    private Persona idPersona;
    
    @JoinColumn(name="IdRol")
    @ManyToOne
    private Rol rol;  
     
    
    // Getters & setters
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String user) {
        this.username = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getcontadorInicioSesion() {
        return contadorInicioSesion;
    }

    public void setContadorInicioSesion(int contadorInicioSesion) {
        this.contadorInicioSesion = contadorInicioSesion;
    }
    
    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
    
    public Persona getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Persona idPersona) {
        this.idPersona = idPersona;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", user=" + username + ", password=" + password + ", estado=" + estado + ", idPersona=" + idPersona + ", rol=" + rol + ", contadorInicioSesion=" + contadorInicioSesion + ", saldo=" + saldo + '}';
    }

    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.idUsuario;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        return true;
    }
    
}

