/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.NoticiasFacadeLocal;
import com.unileon.modelo.Noticias;
import com.unileon.modelo.Usuario;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.ToggleEvent;

/**
 *
 * @author Usuario
 */
@Named
@ViewScoped
public class PrincipalController implements Serializable
{
    
    @EJB
    private NoticiasFacadeLocal noticiasEJB;
    
    private List<Noticias> noticias;
    
     @PostConstruct
    public void inicio(){
    
        this.noticias = this.noticiasEJB.findAll();
         
                
        
        
    }

    public List<Noticias> getNoticias() {
        return noticias;
    }

    public void setNoticias(List<Noticias> noticias) {
        this.noticias = noticias;
    }
    
     public void onClose(CloseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
     
    public void onToggle(ToggleEvent event) {
        
       
        
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    /**
     * Método que devuelve un boolean según el tipo de usuario que seamos.
     * @return true -> Admin | False -> Client
     */
    public boolean isAdmin(){
        
        char admin = Utils.getCurrentUserSession().getRol().getTipoUsuario();
        
        if(admin == 'A')
            return true;
        
        return false;
    }
    
    public  String getAdmin(){
        return Utils.getCurrentUserSession().getUsername();
    }
    
}
