/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */

// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class BloquearUsuarioController implements Serializable {
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    // Lista de usuarios
    private List<Usuario> listaUsuarios;
    
    
    @PostConstruct
    public void inicio(){
    
        this.listaUsuarios = this.usuarioEJB.findAll();
        
    }
    
    // Getters & setters
    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    /**
     * Método para bloquear al usuario -> se cambia su estado
     * @param user
     */
    public void bloquearUser(Usuario user){
    
        try {
            
            // Cambiamos el estado del usuario
            user.setEstado(true);
            
            // Seteamos el contador a un número mayor que 5
            user.setContadorInicioSesion(6);
            
            // Cambiamos el estado del usuario en la BDD
            this.usuarioEJB.edit(user);
        
        
        } catch (Exception e) {
        
            System.out.println("Error al bloquear usuario: "+e.getMessage());
        
        }
    
    }
    
    /**
     * Método para desbloquear al usuario -> se cambia su estado
     * @param user
     */
    public void desbloquearUser(Usuario user){
    
        try {
            
            // Cambiamos el estado del usuario
            user.setEstado(false);
            
            // Seteamos el contador a 0
            user.setContadorInicioSesion(0);
            
            // Cambiamos el estado del usuario en la BDD
            this.usuarioEJB.edit(user);
        
        
        } catch (Exception e) {
        
            System.out.println("Error al desbloquear usuario: "+e.getMessage());
        
        }
    
    }
    
    
} 
