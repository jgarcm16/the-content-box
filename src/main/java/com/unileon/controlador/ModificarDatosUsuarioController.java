/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.PersonaFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Persona;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class ModificarDatosUsuarioController implements Serializable{
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    @EJB
    private PersonaFacadeLocal personaEJB;
    
    private Usuario currentUsuario;
    private Persona currentPersona;
    
    private String nuevoUser;
    private String repiteContraseña;
    
    private List<Usuario> listaUsuarios;

    
    @PostConstruct
    public void inicio(){
    
        this.listaUsuarios = this.usuarioEJB.findAll();
        
        this.repiteContraseña = "";
        
        // Inicializamos el usuario y la persona con el usuario que se encuentre
        // actualmente en la sesión
        this.currentUsuario = (Usuario) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("usuario");
        
        this.nuevoUser = this.currentUsuario.getUsername();

        
        this.currentPersona = this.currentUsuario.getIdPersona();
    
    }

    // Getters & setters
    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public PersonaFacadeLocal getPersonaEJB() {
        return personaEJB;
    }

    public void setPersonaEJB(PersonaFacadeLocal personaEJB) {
        this.personaEJB = personaEJB;
    }

    public Usuario getCurrentUsuario() {
        return currentUsuario;
    }

    public void setCurrentUsuario(Usuario currentUsuario) {
        this.currentUsuario = currentUsuario;
    }

    public Persona getCurrentPersona() {
        return currentPersona;
    }

    public void setCurrentPersona(Persona currentPersona) {
        this.currentPersona = currentPersona;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String getRepiteContraseña() {
        return repiteContraseña;
    }

    public void setRepiteContraseña(String repiteContraseña) {
        this.repiteContraseña = repiteContraseña;
    }

    public String getNuevoUser() {
        return nuevoUser;
    }

    public void setNuevoUser(String nuevoUser) {
        this.nuevoUser = nuevoUser;
    }
    
    
    
    public void modificarPersona(){
    
        try {
            
            this.personaEJB.edit(this.currentPersona);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.currentUsuario.getUsername()
                            +" actualizada con éxito. "));
        
        } catch (Exception e) {
        
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modificando los datos de "
                            + this.currentUsuario.getUsername()));
            
            System.out.println("Error al modificar datos de persona: " + e.getMessage());
        
        }
        
    }
    
    public void modificarUsuario(){
    
        try {
            
            // Comprobamos que las constraseñas coincidan
            if (this.repiteContraseña.equals(this.currentUsuario.getPassword())){
                
                // Comprobamos si se ha cambiado el nombre de usuario en la BDD
                Usuario userSession = (Usuario) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("usuario");
                
                // El usuario no ha modificado el nombre de usuario
                if (this.currentUsuario.getUsername().equals(this.nuevoUser)){
                                    
                    this.usuarioEJB.edit(this.currentUsuario);   
                    FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.currentUsuario.getUsername()
                                +" actualizada con éxito. "));
                    
                    // Actualizamos el valor del usuario logeado
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario",this.currentUsuario);
                
                // El usuario ha modificado el nombre de usuario
                } else {
                
                    // Comprobamos que el nombre elegido no existe en la lista de usuarios
                    boolean existe = false;
                    
                    for (int i = 0; i < this.listaUsuarios.size(); i++){
                        if (this.nuevoUser.equals(this.listaUsuarios.get(i).getUsername()))
                            existe = true;
                    }
                    
                    // El usuario elegido existe en la base de datos y no se puede actualizar
                    if (existe){
                   
                         FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El nombre de usuario ya está registrado en la base"
                                    + "de datos."));
                   
                    // El usuario elegido no existe en la base de datos -> actualizamos los datos del usuarios     
                    } else {
                        
                            // Seteamos el nombre de usuario al usuario
                            this.currentUsuario.setUsername(this.nuevoUser);
                        
                            this.usuarioEJB.edit(this.currentUsuario);   
                            FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.currentUsuario.getUsername()
                                    +" actualizada con éxito!. "));
                            
                            // Actualizamos el valor del usuario logeado
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario",this.currentUsuario);

                    }
                }
            
                
            } else {
            
                FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Las contraseñas deben coincidir."));
            
            }
        
        } catch (Exception e) {
        
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modificando los datos de "
                            + this.currentUsuario.getUsername()));
            
            System.out.println("Error al modificar datos de usuario: " + e.getMessage());
        
        }
        
    }
    
}
