/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.MisProductosFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Producto;
import com.unileon.modelo.Usuario;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@SessionScoped
public class CarritoController implements Serializable{
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    @EJB
    private MisProductosFacadeLocal misProductosEJB;
    
    private List<Producto> listaProductos;
    private List<Integer> listaPrecios;
    private List<Character> listaTipos;
    
    private int numProductos;
    private int total;
    
    @PostConstruct
    public void inicio(){
            
        this.listaProductos = new ArrayList<>();
        this.listaPrecios = new ArrayList<>();
        this.listaTipos = new ArrayList<>();
        
        this.calcularNumProductos();
        
        this.calcularTotal();
        
    }
    
    // Getters & setters

    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public MisProductosFacadeLocal getMisProductosEJB() {
        return misProductosEJB;
    }

    public void setMisProductosEJB(MisProductosFacadeLocal misProductosEJB) {
        this.misProductosEJB = misProductosEJB;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Integer> getListaPrecios() {
        return listaPrecios;
    }

    public void setListaPrecios(List<Integer> listaPrecios) {
        this.listaPrecios = listaPrecios;
    }

    public int getNumProductos() {
        return numProductos;
    }

    public void setNumProductos(int numProductos) {
        this.numProductos = numProductos;
    }

    public List<Character> getListaTipos() {
        return listaTipos;
    }

    public void setListaTipos(List<Character> listaTipos) {
        this.listaTipos = listaTipos;
    }
    
    

    /**
     * Método que calcula el precio total de los productos
     */
    public void calcularTotal() {
        
        this.total = 0;

        if (this.listaPrecios.isEmpty())
            this.total = 0;
        else {
        
            for (int i = 0; i < this.listaPrecios.size(); i++)
                this.total += this.listaPrecios.get(i);
        }

    }

    /**
     * Método que calcula el número total de productos en el carrito
     */
    private void calcularNumProductos() {
        
        this.numProductos = this.listaProductos.size();
        
    }
    
    /**
     * Método que devuelve si se debe rednerizar o no el número de elementos
     * @return true si se puede renderizar, false si no
     */
    public boolean renderNumProductos(){
    
        // Se podrá renderizar el número de elementos en el carrito
        // cuando haya algún elemento en el carrito
            return this.numProductos != 0;
        
    }
    
    /**
     * Método para añadir una compra al carrito
     * @param producto 
     */
    public void anadirCompra(Producto producto){

        if (!this.isEnCarrito(producto)){
            
            // Añadimos el producto a la lista de productos
            this.listaProductos.add(producto);
            
            // Añadimos el precio a la lista de precios
            this.listaPrecios.add(producto.getPrecioCompra());
            
            // Añadimos el tipo de compra/alquiler
            this.listaTipos.add('C');
            
            // Calculamos el nuevo número de productos
            this.calcularNumProductos();
        
            // Calculamos el precio todoal
            this.calcularTotal();
            
        } else 
             System.out.println("Error al añadir compra al carrito");   
        
    }
    
    public void anadirAlquiler(Producto producto){
    
        if (!this.isEnCarrito(producto)){
            
            // Añadimos el producto a la lista de productos
            this.listaProductos.add(producto);
            
            // Añadimos el precio a la lista de precios
            this.listaPrecios.add(producto.getPrecioAlquiler());
            
            // Añadimos el tipo de compra/alquiler
            this.listaTipos.add('A');
            
            // Calculamos el nuevo número de productos
            this.calcularNumProductos();
        
            // Calculamos el precio todoal
            this.calcularTotal();
            
        } else 
             System.out.println("Error al añadir alquiler al carrito");   
    
    }


    /**
     * Método para comprobar si un producto está en el carrito o no
     * @param producto
     * @return 
     */
    public boolean isEnCarrito(Producto producto) {
        
        for (int i = 0; i < this.listaProductos.size(); i++){
                    
            if (producto.equals(this.listaProductos.get(i))){
                return true;
            }
            
        }
        
        return false;

    }
    
    /**
     * Método que devuelve el índice del producto dentro de la lista de productos
     * del carrito
     * @param producto 
     */
    private int getProductIndex(Producto producto){
    
        for (int i = 0; i < this.listaProductos.size(); i++){
        
            if (this.listaProductos.get(i).equals(producto))
                return i;
            
        }
        
        return -1;
        
    }
    
    /**
     * Método que comprueba si el producto que se ha añadido a la lista
     * de productos del carrito comprándolo
     * @param producto
     * @return 
     */
    public boolean isCompra(Producto producto){
    
        int indiceProducto = this.getProductIndex(producto);
        
        if (indiceProducto == -1)
            return false;
        else {
        
            return (this.listaTipos.get(indiceProducto) == 'C');
            
        }
        
    }
    
    /**
     * Método que comprueba si el producto que se ha añadido a la lista
     * de productos del carrito alquiándolo
     * @param producto
     * @return 
     */
    public boolean isAlquiler(Producto producto){
    
        int indiceProducto = this.getProductIndex(producto);
        
        if (indiceProducto == -1)
            return false;
        else {
        
            return (this.listaTipos.get(indiceProducto) == 'A');
            
        }
    
    }
    
    /**
     * Método que elimina el producto pasado como parámetro de la lista de
     * productos del carrito y la actualiza
     * @param producto 
     */
    public void deleteProducto(Producto producto){
        
        
        // Obtenemos el índice el producto
        int indice = this.getProductIndex(producto);
        
        if (indice != -1){
        
            // Eliminamos el producto de la lista de productos
            this.listaProductos.remove(indice);

            // Eliminamos el tipo de compra de la lista de tipos
            this.listaTipos.remove(indice);

            // Eliminamos el precio de la lista de precios
            this.listaPrecios.remove(indice);

            // Actualizamos los valores
            this.calcularNumProductos();

            this.calcularTotal();
            
        } else 
            System.out.println("Error eliminar producto");
        
    }
    
    /**
     * Método que devuelve el nombre de usuario del usuario de la sesión
     * @return 
     */
    public String getUsername(){
    
        return Utils.getCurrentUserSession().getUsername();
        
    }
    
    /**
     * Método que devuelve el saldo del usuario de la sesión
     * @return 
     */
    public int getSaldo(){
    
        return Utils.getCurrentUserSession().getSaldo();
        
    }
    
    /**
     * Método que decuelve boolean para que se pueda realizar el procesamiento de 
     * la compra/alquler o no
     * @return 
     */
    public boolean renderProcesar(){
    
        /**
         * Devuelve true (botón bloqueado) si:
         *  - el saldo del usuario es inferior al total
         * OR
         *  - el número de productos es 0 (no hay ningún producto)
         * */
        
        return ((this.saldoInsuficiente()) || (this.numProductos == 0));
    
    }
    
    /**
     * Método que devuelve un boolean indicando si el usuario dispone del saldo
     * suficiente para realizar la compra o no
     * @return 
     */
    public boolean saldoInsuficiente(){
    
        return (Utils.getCurrentUserSession().getSaldo() < this.total);
        
    }
    
    /**
     * Método para calcular el saldo restante tras la compra/alquiler
     * @return 
     */
    public int calcularRestante(){
    
        return (Utils.getCurrentUserSession().getSaldo() - total);
    
    }
    
    private boolean isCompra(char tipo){
    
        return tipo == 'C';
    
    }
    
    /**
     * Método que procesa la compra del usuario
     */
    public void procesarCompra() {
        
        try{
        
            for (int i = 0; i < this.listaProductos.size(); i++){

                if (isCompra(this.listaTipos.get(i))){

                    comprar(this.listaProductos.get(i));

                } else {

                    alquilar(this.listaProductos.get(i));

                }

            }
            
            // Al salir del bucle debemos restar al usuario el coste de la compra
            this.restarCompraUsuario();
            
            // Reinicializamos las listas
            this.inicio();
                        
        } catch (Exception e) {
        
            System.out.println("Error al procesar compra: "+e.getMessage());
            
        }
    
    }

    /**
     * Método que procesa la compra de un producto
     * @param producto 
     */
    private void comprar(Producto producto) {

        // Creamos el objeto
        MisProductos misProductos = new MisProductos();
        
        // Seteamos el producto 
        misProductos.setIdProducto(producto);
        
        // Seteamos el usuario que lo va a adquirir
        misProductos.setIdUsuario(Utils.getCurrentUserSession());
        
        // Seteamos el tipo de compra que es
        misProductos.setAlquilado(false);
        
        // Seteamos la fecha de adquisición
        misProductos.setFechaAdquisicion(new Date());
        
        // Seteamos el estado 
        misProductos.setEstado(true);
                
        // Introducimos el producto a la BDD
        this.misProductosEJB.create(misProductos);
                
    }

    /**
     * Método que procesa el alquiler de un producto
     * @param producto 
     */
    private void alquilar(Producto producto) {
        
        // Creamos el objeto
        MisProductos misProductos = new MisProductos();
        
        // Seteamos el producto 
        misProductos.setIdProducto(producto);
        
        // Seteamos el usuario que lo va a adquirir
        misProductos.setIdUsuario(Utils.getCurrentUserSession());
        
        // Seteamos el tipo de compra que es
        misProductos.setAlquilado(true);
        
        // Seteamos los días de alquiler
        misProductos.setDiasAlquiler(producto.getFechaAlquiler());
     
        // Seteamos la fecha de adquisición
        misProductos.setFechaAdquisicion(new Date());
        
        // Calculamos la fecha de expiración y la seteamos
        Date fechaExpiracion = Utils.calcularFecha(producto.getFechaAlquiler());
        misProductos.setFechaExpiracion(fechaExpiracion);
        
        // Seteamos el estado 
        misProductos.setEstado(true);
        
        // Introducimos el producto a la BD
        this.misProductosEJB.create(misProductos);
        
    }

    /**
     * Método que resta al usuario el valor total de la compra
     */
    private void restarCompraUsuario() {
        
        try {

            // Obtenemos el usuario
            Usuario user = Utils.getCurrentUserSession();

            // Calculamos el nuevo saldo del usuario
            int nuevoSaldo = user.getSaldo() - this.total;

            // Seteamos al usuario su nuevo saldo
            user.setSaldo(nuevoSaldo);

            // Modificamos el usuario en la BDD
            this.usuarioEJB.edit(user);
            
        } catch (Exception e) {
    
            System.out.println("Error al restar saldo al usuario: " + e.getMessage());
            
        }
    }
    
    public boolean isMisProductos(Producto producto){
    
        try {
            
            return this.misProductosEJB.isMisProductos(Utils.getCurrentUserSession(), producto);
        
        } catch (Exception e) {
        
            System.out.println("Error al comprobar mis productos: "+e.getMessage());
            return false;
        
        }
        
    
    }
    
    
}
