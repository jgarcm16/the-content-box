/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.PersonaFacadeLocal;
import com.unileon.EJB.ProductoFacadeLocal;
import com.unileon.EJB.RolFacadeLocal;
import com.unileon.EJB.TipoProductoFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Persona;
import com.unileon.modelo.Producto;
import com.unileon.modelo.Rol;
import com.unileon.modelo.TipoProducto;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author msperl00
 */
@Named
@ViewScoped
public class CrearProductoController implements Serializable{
   @EJB
   private ProductoFacadeLocal productoEJB;
   
   @EJB
   private TipoProductoFacadeLocal TipoProductoEJB;
   
   
   
   @Inject
   private Producto producto;
   @Inject
   private TipoProducto tipoProducto;
   
      private List<Producto> productos;
      private List<TipoProducto> tipoProductos;
   
   @PostConstruct
   public void inicio(){
   
        this.productos = productoEJB.findAll();
        this.tipoProductos = TipoProductoEJB.findAll();
   
   }
   /**
    * Metodo que crea el producto con los valores que se ha instanciado.
    */
   public void crearProducto(){
       //Seteamos el tipo de producto
        System.out.println(tipoProducto);
       this.producto.setIdTipoProducto(tipoProducto);
      
       if(siExiste()){
             FacesContext.getCurrentInstance().addMessage("messages",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya existe"));
       }else{
           this.productoEJB.create(this.producto);
           FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Producto creado con éxito"));
       }
       
   }

    public ProductoFacadeLocal getProductoEJB() {
        return productoEJB;
    }

    public void setProductoEJB(ProductoFacadeLocal productoEJB) {
        this.productoEJB = productoEJB;
    }

    public TipoProductoFacadeLocal getTipoProductoEJB() {
        return TipoProductoEJB;
    }

    public void setTipoProductoEJB(TipoProductoFacadeLocal TipoProductoEJB) {
        this.TipoProductoEJB = TipoProductoEJB;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public TipoProducto getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public List<TipoProducto> getTipoProductos() {
        return tipoProductos;
    }

    public void setTipoProductos(List<TipoProducto> tipoProductos) {
        this.tipoProductos = tipoProductos;
    }
    
    public boolean siAlquiler(){
        
        return producto.isAlquiler();
    }
    
     public void addMessage() {
        String summary = producto.isAlquiler() ? "Se alquila el producto" : "No se alquila";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
    }

    private boolean siExiste() {
        
        for(int i = 0; i < this.productos.size();i++){
            if(producto.equals(productos.get(i)))
                return true;
        }
        
        
       return false;
        
    }
   
}
