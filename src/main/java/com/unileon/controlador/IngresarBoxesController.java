/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class IngresarBoxesController implements Serializable{
    
    // Lista de usuarios
    List<Usuario> listaUsuarios;
    
    // Usuario seleccionado
    @Inject
    private Usuario usuario;
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    // Acción que se va a llevar a cabo
    // I -> Ingresar / R -> Retirar
    private String accion;
    
    // Boxes que vamos a ingresar
    private int saldo;
    
    @PostConstruct
    public void inicio(){
        
        this.accion = "R";  // Por ejemplo
        this.listaUsuarios = this.usuarioEJB.findAll();
    
    }
    
    // Getters & setters
    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
    
    
    
    /**
     * Método para ingresar boxes al usuario
     */
    public void ingresar(){
    
    
        try {
            
            // Sumamos el saldo
            this.saldo = saldo + this.usuario.getSaldo();
            
            // Seteamos el saldo al usuario
            this.usuario.setSaldo(this.saldo);
            
            // Modificamos en la BDD
            this.usuarioEJB.edit(this.usuario);
            
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se han ingresado "+this.saldo+""
                        + " correctamente a "+ this.usuario.getUsername()));
            
            // Reseteamos el valor del saldo
            this.saldo = 0;
            
                    
        
        } catch (Exception e) {
        
            System.out.println("Error al ingresar boxes: "+e.getMessage());
            
        }
        
    }
    
    /**
     * Método para retirar boxes al usurio
     */
    public void retirar(){
        
        try {
            
            // Comprobamos que el saldo que queremos retirar no es mayor que 
            // el saldo del que dispone el usuario
            if (this.saldo > this.usuario.getSaldo()){
            
                FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario no puede quedarse con un "
                        + "saldo negativo."));
                
            } else {
            
                // Seteamos el nuevo saldo saldo 
                this.usuario.setSaldo(this.usuario.getSaldo() - this.saldo);

                // Modificamos en la BDD
                this.usuarioEJB.edit(this.usuario);

                FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se han retirado "+this.saldo+""
                            + " correctamente a "+ this.usuario.getUsername()));

                // Reseteamos el valor del saldo
                this.saldo = 0;
            
            }
        
        
        } catch (Exception e) {
        
            System.out.println("Error al retirar boxes: "+e.getMessage());

        }
    
    }
    
    /**
     * Establecer el usuario que se ha pasado en la tabla y settear
     * el tipo de acción a I
     * @param usr 
     */
    public void establecerUsuarioIngresar(Usuario usr){
    
        this.usuario = usr;
        this.accion = "I";
        
    }
    
    /**
     * Establecer el usuario que se ha pasado en la tabla y settear
     * el tipo de acción a R
     * @param usr 
     */
    public void establecerUsuarioRetirar(Usuario usr){
    
        this.usuario = usr;
        this.accion = "R";
        
    }
    
    
}
