/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.MensajeFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Mensaje;
import com.unileon.modelo.Usuario;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class MensajesController implements Serializable{
    
    @EJB
    private MensajeFacadeLocal mensajeEJB;
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    @Inject
    private Usuario receptor;
    @Inject
    private Mensaje mensaje;
    
    private String textoBusqueda;
    private List<String> autocompletado;
    
    private List<Mensaje> buzonEntrada;
    
    private List<Mensaje> buzonEntradaUsuarios;
    
    private List<Usuario> listaUsuarios;
    
    @PostConstruct
    public void inicio(){
    
        this.buzonEntrada = this.mensajeEJB.obtenerMensajesUsuario(Utils.getCurrentUserSession());
        
         this.buzonEntradaUsuarios = this.mensajeEJB.findAll();
        
        this.textoBusqueda = "";
        this.autocompletado = new ArrayList<String>();
        
        this.listaUsuarios = this.usuarioEJB.findAll();
        
    }
    
    // Getters & setters
    public List<Mensaje> getBuzonEntrada() {
        return buzonEntrada;
    }

    public void setBuzonEntrada(List<Mensaje> buzonEntrada) {
        this.buzonEntrada = buzonEntrada;
    }
    ////
    public List<Mensaje> getBuzonEntradaUsuarios() {
        return buzonEntradaUsuarios;
    }

    public void setBuzonEntradaUsuarios(List<Mensaje> buzonEntradaUsuarios) {
        this.buzonEntradaUsuarios = buzonEntradaUsuarios;
    }

    public Usuario getReceptor() {
        return receptor;
    }

    public void setReceptor(Usuario receptor) {
        this.receptor = receptor;
    }

    public MensajeFacadeLocal getMensajeEJB() {
        return mensajeEJB;
    }

    public void setMensajeEJB(MensajeFacadeLocal mensajeEJB) {
        this.mensajeEJB = mensajeEJB;
    }

    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public String getTextoBusqueda() {
        return textoBusqueda;
    }

    public void setTextoBusqueda(String textoBusqueda) {
        this.textoBusqueda = textoBusqueda;
    }

    public List<String> getAutocompletado() {
        return autocompletado;
    }

    public void setAutocompletado(List<String> autocompletado) {
        this.autocompletado = autocompletado;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    
    
    /**
     * Método para autocompletar la búsqueda
     * @param query
     * @return 
     */
    public List<String> metodoAutocompletado(String query) {
        
        this.autocompletado = new ArrayList<String>();
                
        for (int i = 0; i < this.listaUsuarios.size(); i++){
        
            if (Utils.stripAccents(this.listaUsuarios.get(i).getUsername().toLowerCase()).contains(Utils.stripAccents(query.toLowerCase())))
                autocompletado.add(this.listaUsuarios.get(i).getUsername());
        
        }
        
        return this.autocompletado;
    }
        
    /**
     * Método para enviar un mensaje
     */
    public void enviarMensaje(){
    
        if (this.validarReceptor()){
            
            try {
        
            // Seteamos el emisor al mensaje
            this.mensaje.setEmisor(Utils.getCurrentUserSession());
            
            // Seteamos el receptor al mensaje
            this.mensaje.setReceptor(this.receptor);
            
            // Seteamos el estado (leido) a false
            this.mensaje.setEstado(false);
            
            // Seteamos la fecha
            this.mensaje.setFecha(new Date());
            
            // Insertamos el mensaje en la BDD
            this.mensajeEJB.create(this.mensaje);

            // Mosntrar mensaje idnicando algo tipo: Mensaje enviado a <usuario>
            // de forma correcta! y reinicializar los atributos
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Mensaje enviado a "+this.receptor.getUsername()
                            +" de forma correcta!"));

            // Reinicializamos atributos
            this.receptor = new Usuario();
            this.mensaje = new Mensaje();
            
            } catch (Exception e) {
            
                FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error al enviar el mensaje."));
                
                System.out.println("Error al enviar mensaje: "+ e.getMessage());
            
            }   
        }  
    }

    /**
     * Método para validar el destintario del mensaje
     * @return 
     */
    private boolean validarReceptor() {
        
        // Si el receptor tiene un username que no existe
        if (!this.comprobarUsernameValido()){
            
            // Mostrar mensaje diciendo que el usuario insertado no se encuentra
            // registrado en el sistema o que es un usuario inválido
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario indicado como destinatario no existe en el sistema."));
            
            return false;
        }
        
        // Obtenemos el usuario que coincide con el nombre de usuario
        this.receptor = (Usuario) this.usuarioEJB.buscarUsername(this.receptor.getUsername());
        return true;
        
    }

    private boolean comprobarUsernameValido() {

        for (int i = 0; i < this.listaUsuarios.size(); i++){
            
            if (this.listaUsuarios.get(i).getUsername().equals(this.receptor.getUsername()))
                return true;
            
        }
        
        return false;

    }
    
    /**
     * Método para devolver el método acortado con puntos suspensivos
     * @param text
     * @param num
     * @return 
     */
    public String truncateText(String text, int num){
        
        if (text.length() >= num){
        
            return (text.substring(0, num-3).concat("..."));
            
        } else return text;
    
    }
    
    /**
     * Método para imprimir la fecha en formato dd-MM-aaaa
     * @param fecha
     * @return 
     */
    public String fechaFormateada (Date fecha){
    
        return Utils.fechaFormateada(fecha);
        
        
    }
    
    /**
     * Método para cambiar el estado de los mensajes cuando se lean
     * @param estado 
     */
    public void cambiarEstado(boolean estado){
                
        try {
    
            boolean noLeido = !estado;

            if (noLeido){

                // Seteamos el nuevo estado
                this.mensaje.setEstado(true);

                // Lo insertamos en la BDD
                this.mensajeEJB.edit(this.mensaje);

            }
            
        } catch (Exception e) {
            
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error al leer el mensaje."));
        
            System.out.println("Error al cambiar el estado: "+e.getMessage());
        
        }
    
    }
    
    
    
    
}
