/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.PersonaFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Persona;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */

// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class EliminarUsuarioController implements Serializable{
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    @EJB
    private PersonaFacadeLocal personaEJB;
    
    private Usuario currentUsuario;
    private Persona currentPersona;
    
    @PostConstruct
    public void inicio(){
    
        // Inicializamos el usuario y la persona con el usuario que se encuentre
        // actualmente en la sesión
        this.currentUsuario = (Usuario) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("usuario");
        
        this.currentPersona = this.currentUsuario.getIdPersona();
    
    }

    public void eliminarUsuario(){
        
        try {
           
            this.usuarioEJB.remove(this.currentUsuario);
            this.personaEJB.remove(this.currentPersona);
         
        } catch (Exception e) {
        
            // Tratamiento del error
            System.out.println("Error al eliminar al usuario: "+e.getMessage());
            
        }
        
    }
    
}
