/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.FavoritoFacadeLocal;
import com.unileon.EJB.MisProductosFacadeLocal;
import com.unileon.modelo.Favorito;
import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Producto;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class MisProductosController implements Serializable{
    
    @EJB
    private FavoritoFacadeLocal favoritoEJB;
    
    @EJB
    private MisProductosFacadeLocal misProductosEJB;

    private List<MisProductos> listaProductos;
    
    // Producto seleccionado
    @Inject
    private Producto producto;
    
    // Favorito insertado
    @Inject
    private Favorito favorito;
    
    @PostConstruct
    public void inicio(){

        this.listaProductos = misProductosEJB.obtenerProductosUsuario(Utils.getCurrentUserSession());

    }
   
    // Getters & setters
    public FavoritoFacadeLocal getFavoritoEJB() {
        return favoritoEJB;
    }

    public void setFavoritoEJB(FavoritoFacadeLocal favoritoEJB) {
        this.favoritoEJB = favoritoEJB;
    }

    public MisProductosFacadeLocal getMisProductosEJB() {
        return misProductosEJB;
    }

    public void setMisProductosEJB(MisProductosFacadeLocal misProductosEJB) {
        this.misProductosEJB = misProductosEJB;
    }

    public List<MisProductos> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<MisProductos> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Favorito getFavorito() {
        return favorito;
    }

    public void setFavorito(Favorito favorito) {
        this.favorito = favorito;
    }
    
    
   
   /**
    * Método que devuelve en formato String el número de días de alquiler restantes
    * @param producto
    * @return 
    */ 
    public String calcularDiasRestantes(MisProductos producto){
        
        // Si no está caducado
        if (producto.isEstado()){
            
            long dias = Utils.calcularDias(new Date(), producto.getFechaExpiracion());
            
            return String.valueOf(dias) + " día(s) restante(s)";
        
            
        // Si está caducado
        } else {
            
            return "caducado";
        
        }
    
    }
    
    public boolean esFavorito(Producto prod){
    
        return this.favoritoEJB.buscarFavoritoUser(Utils.getCurrentUserSession(), prod);
        
    }
    
    public void anadirFavorito(){
        
            
            // Consultamos si el usuario ya tiene el producto en la BDD
            Favorito aux = new Favorito();
           
            if (this.favoritoEJB.buscarFavoritoUser(Utils.getCurrentUserSession(), this.producto)){
                FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "El producto "+this.producto.getTitulo()+" "
                                + "ya se encuentra en su lista de favoritos."));
            } else {
        
                // Seteamos el producto a la lista de favoritos
                this.favorito.setIdProducto(this.producto);

                // Seteamos el usuario a la lista de favoritos
                this.favorito.setIdUsuario(Utils.getCurrentUserSession());
                               
                try {

                // Insertamos en la BDD
                this.favoritoEJB.create(this.favorito);

                 FacesContext.getCurrentInstance().addMessage("messages",
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Se ha guardado "+this.producto.getTitulo()+" "
                                    + "en la lista de favoritos."));
                 
                 } catch (Exception e) {
        
                    System.out.println("Error añadiendo a favoritos: "+ e.getMessage());

                }
            }
            
      
    }
    
}
