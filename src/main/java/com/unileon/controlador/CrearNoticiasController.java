/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.CatNoticiasFacade;
import com.unileon.EJB.CatNoticiasFacadeLocal;
import com.unileon.EJB.NoticiasFacadeLocal;
import com.unileon.EJB.ProductoFacadeLocal;
import com.unileon.EJB.TipoProductoFacadeLocal;
import com.unileon.modelo.CatNoticias;
import com.unileon.modelo.Noticias;
import com.unileon.modelo.Producto;
import com.unileon.modelo.TipoProducto;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Usuario
 */
@Named
@ViewScoped
public class CrearNoticiasController implements Serializable {

    @EJB
    private NoticiasFacadeLocal noticiaEJB;

    @EJB
    private CatNoticiasFacadeLocal CatNoticiaEJB;

    
   @Inject
   private Noticias noticia;
   @Inject
   private CatNoticias CatNoticia;
   
     private List<Noticias> noticias;
     private List<CatNoticias> tipoNoticias;
     
     @PostConstruct
   public void inicio(){
   
        this.noticias = noticiaEJB.findAll();
        this.tipoNoticias = CatNoticiaEJB.findAll();
   
   }
   
   public void crearNoticias(){
       //Seteamos el tipo de producto

       this.noticia.setCategoriaId(CatNoticia);
       this.noticia.setUsuario(Utils.getCurrentUserSession());
       
       if(siExiste()){
             FacesContext.getCurrentInstance().addMessage("messages",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya existe"));
       }else{
           this.noticiaEJB.create(noticia);
           FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Producto creado con éxito"));
       }
   }

    public NoticiasFacadeLocal getNoticiaEJB() {
        return noticiaEJB;
    }

    public void setNoticiaEJB(NoticiasFacadeLocal noticiaEJB) {
        this.noticiaEJB = noticiaEJB;
    }

    public CatNoticiasFacadeLocal getCatNoticiaEJB() {
        return CatNoticiaEJB;
    }

    public void setCatNoticiaEJB(CatNoticiasFacadeLocal CatNoticiaEJB) {
        this.CatNoticiaEJB = CatNoticiaEJB;
    }

    public Noticias getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticias noticia) {
        this.noticia = noticia;
    }

    public CatNoticias getCatNoticia() {
        return CatNoticia;
    }

    public void setCatNoticia(CatNoticias CatNoticia) {
        this.CatNoticia = CatNoticia;
    }

    public List<Noticias> getNoticias() {
        return noticias;
    }

    public void setNoticias(List<Noticias> noticias) {
        this.noticias = noticias;
    }

    public List<CatNoticias> getTipoNoticias() {
        return tipoNoticias;
    }

    public void setTipoNoticias(List<CatNoticias> tipoNoticias) {
        this.tipoNoticias = tipoNoticias;
    }

    private boolean siExiste() {
         for(int i = 0; i < this.noticias.size();i++){
            if(noticia.equals(noticias.get(i)))
                return true;
        }
        
        
       return false;
    }
     
}
