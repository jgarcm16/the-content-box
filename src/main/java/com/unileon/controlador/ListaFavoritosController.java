/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.FavoritoFacadeLocal;
import com.unileon.modelo.Favorito;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class ListaFavoritosController implements Serializable{
    
    @EJB
    private FavoritoFacadeLocal favoritoEJB;
    
    private List<Favorito> listaFavoritos;
    
    @PostConstruct
    public void inicio(){
    
        this.listaFavoritos = this.favoritoEJB.obtenerFavoritosUser(Utils.getCurrentUserSession());  
    
    }
    
    
    // Getters & setters
    public FavoritoFacadeLocal getFavoritoEJB() {
        return favoritoEJB;
    }

    public void setFavoritoEJB(FavoritoFacadeLocal favoritoEJB) {
        this.favoritoEJB = favoritoEJB;
    }

    public List<Favorito> getListaFavoritos() {
        return listaFavoritos;
    }

    public void setListaFavoritos(List<Favorito> listaFavoritos) {
        this.listaFavoritos = listaFavoritos;
    }
    
    // Métodos propios
    
    /**
     * Método para editar un Favorito
     * @param fav 
     */
    public void modificarDatos(Favorito fav){
        
        try{
            
            this.favoritoEJB.edit(fav);

            
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Favorito modificado con éxito."));
            
            // Actualizamos la lista
            this.listaFavoritos = this.favoritoEJB.obtenerFavoritosUser(Utils.getCurrentUserSession());
        
        
        } catch (Exception e) {
        
            System.out.println("Error modificando favorito: " +e.getMessage());
            
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modidificando el Favorito."));
        
        }
    
           
    }
    
    /**
     * Método para eliminar un Favorito
     * @param fav 
     */
    public void eliminarFavorito(Favorito fav){
        
        try{
            
            this.favoritoEJB.remove(fav);

            
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Favorito eliminado con éxito."));
            

            // Actualizamos la lista
            this.listaFavoritos = this.favoritoEJB.obtenerFavoritosUser(Utils.getCurrentUserSession());
        
        
        } catch (Exception e) {
        
            System.out.println("Error eliminando favorito: " +e.getMessage());
            
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error eliminando el Favorito."));
        
        }
    
    
    }
    
    
    
}
