/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.CatNoticiasFacadeLocal;
import com.unileon.EJB.NoticiasFacadeLocal;
import com.unileon.modelo.CatNoticias;
import com.unileon.modelo.Noticias;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.rmi.CORBA.Util;

/**
 *
 * @author Marco Speranza López
 */
@Named
@ViewScoped
public class NoticiasController implements Serializable{
    
    private List<Noticias> noticias;
    private List<Noticias> noticiasFiltradas;
    private List<CatNoticias> categorias;
    
    @EJB
    private NoticiasFacadeLocal noticiasEJB;
    
    @EJB
    private CatNoticiasFacadeLocal categoriasEJB;
    /**
     *
     */
    @PostConstruct
    public void inicio(){
        noticias = this.noticiasEJB.findAll();
        categorias = this.categoriasEJB.findAll();
        noticiasFiltradas = new ArrayList<Noticias>();
        
    }
    
    public List<Noticias> getNoticias() {
        return noticias;
    }

    
    public void setNoticias(List<Noticias> noticias) {
        this.noticias = noticias;
    }

    public List<Noticias> getNoticiasFiltradas() {
        return noticiasFiltradas;
    }

    public void setNoticiasFiltradas(List<Noticias> noticiasFiltradas) {
        this.noticiasFiltradas = noticiasFiltradas;
    }

    public List<CatNoticias> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CatNoticias> categorias) {
        this.categorias = categorias;
    }

    public NoticiasFacadeLocal getNoticiasEJB() {
        return noticiasEJB;
    }

    public void setNoticiasEJB(NoticiasFacadeLocal noticiasEJB) {
        this.noticiasEJB = noticiasEJB;
    }

    public CatNoticiasFacadeLocal getCategoriasEJB() {
        return categoriasEJB;
    }

    public void setCategoriasEJB(CatNoticiasFacadeLocal categoriasEJB) {
        this.categoriasEJB = categoriasEJB;
    }
    
    public List<Noticias> devolverCat(CatNoticias categoria){
        
         List<Noticias> noticiasPorCategorias = new ArrayList<Noticias>();
        
        for (Iterator<Noticias> iterator = noticias.iterator(); iterator.hasNext();) {
            Noticias next = iterator.next();
           
            if(categoria.equals(next.getCategoriaId()))
                noticiasPorCategorias.add(next);
            
        }
        return noticiasPorCategorias;
    }
    
    
    
}
