/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.FavoritoFacadeLocal;
import com.unileon.EJB.MisProductosFacadeLocal;
import com.unileon.EJB.ProductoFacadeLocal;
import com.unileon.modelo.Favorito;
import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Producto;
import com.unileon.utils.Utils;
import java.io.Serializable;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@SessionScoped
public class TodosProductosController implements Serializable{
    
    @EJB
    private ProductoFacadeLocal productoEJB;
    
    @EJB
    private FavoritoFacadeLocal favoritoEJB;
    
    @EJB
    private MisProductosFacadeLocal misProductosEJB;
    
    private List<Producto> listaProductos;
    private List<Producto> listaProductosMostrados;
   
    private List<Producto> listaProductosBuscador;
    
    private String textoBusqueda;
    private List<String> autocompletado;
    
    private DataTable theTable = new DataTable();
    private final Map<String, Object> theFilterValues; 

    public TodosProductosController() {
        this.theFilterValues = theTable.getFilters();
   
    }
         
    // Producto seleccionado
    @Inject
    private Producto producto;
    
    // Favorito insertado
    @Inject
    private Favorito favorito;
    
    @PostConstruct
    public void inicio(){
    
        this.listaProductos = this.productoEJB.findAll();
        this.listaProductosMostrados = this.listaProductos;
        
       
        
        this.textoBusqueda = "";
        this.autocompletado = new ArrayList<String>();
        
        // Desordenamos los productos para que salgan de manera aleatoria
        Collections.shuffle(this.listaProductos);
    
    }
    
     
    public DataTable getTheTable() {
        return theTable;
    }

    public void setTheTable(DataTable theTable) {
        this.theTable = theTable;
    }

    public List<Producto> getListaProductosBuscador() {
        return listaProductosBuscador;
    }

    public void setListaProductosBuscador(List<Producto> listaProductosBuscador) {
        this.listaProductosBuscador = listaProductosBuscador;
    }
    
    // Getters & setters
    public ProductoFacadeLocal getProductoEJB() {
        return productoEJB;
    }

    public void setProductoEJB(ProductoFacadeLocal productoEJB) {
        this.productoEJB = productoEJB;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getTextoBusqueda() {
        return textoBusqueda;
    }

    public void setTextoBusqueda(String textoBusqueda) {
        this.textoBusqueda = textoBusqueda;
    }

    public List<String> getAutocompletado() {
        return autocompletado;
    }

    public void setAutocompletado(List<String> autocompletado) {
        this.autocompletado = autocompletado;
    }

    public List<Producto> getListaProductosMostrados() {
        return listaProductosMostrados;
    }

    public void setListaProductosMostrados(List<Producto> listaProductosMostrados) {
        this.listaProductosMostrados = listaProductosMostrados;
    }

    public FavoritoFacadeLocal getFavoritoEJB() {
        return favoritoEJB;
    }

    public void setFavoritoEJB(FavoritoFacadeLocal favoritoEJB) {
        this.favoritoEJB = favoritoEJB;
    }

    public Favorito getFavorito() {
        return favorito;
    }

    public void setFavorito(Favorito favorito) {
        this.favorito = favorito;
    }

    public MisProductosFacadeLocal getMisProductosEJB() {
        return misProductosEJB;
    }

    public void setMisProductosEJB(MisProductosFacadeLocal misProductosEJB) {
        this.misProductosEJB = misProductosEJB;
    }
    
    
    /**
     * Nos devuelve el nº de elementos contenidos en el lista de productos de busqueda,
     * pertenecientes a la página principal.
     * @return 
     */
    public int lenght(){
        
//        if(!this.listaProductosBuscador.equals(null))
//       return listaProductosBuscador.size();

        return 0;
    }
    
    
    

    // Método para autocompletar búsqueda
    public List<String> metodoAutocompletado(String query) {
        
        this.autocompletado = new ArrayList<String>();
                
        for (int i = 0; i < this.listaProductos.size(); i++){
        
            if (Utils.stripAccents(this.listaProductos.get(i).getTitulo().toLowerCase()).contains(Utils.stripAccents(query.toLowerCase())))
                autocompletado.add(this.listaProductos.get(i).getTitulo());
        
        }
        
        return this.autocompletado;
    }
    
    // Evento para producto seleccionado
    public void onItemSelect(SelectEvent event) {

        if (this.productoEJB.buscarPorNombre((String)event.getObject()) == null)
            System.out.println("Error obteniento.");
        else this.producto = this.productoEJB.buscarPorNombre((String)event.getObject());

    }
    
    public void actualizarListaBusqueda(){
       
        if (this.autocompletado.size() == 0){
        
            this.listaProductosMostrados = new ArrayList<Producto>();
        
        } else {
        
            List<Producto> nuevaLista = new ArrayList<Producto>();
            
            for (int i = 0; i < this.autocompletado.size(); i++){
                
                Producto p = this.productoEJB.buscarPorNombre(this.autocompletado.get(i));
                nuevaLista.add(p);
             
            }
        
            this.listaProductosMostrados = nuevaLista;
            
        }

    }
    
    public void anadirFavorito(){
        
            
            // Consultamos si el usuario ya tiene el producto en la BDD
            Favorito aux = new Favorito();
           
            if (this.favoritoEJB.buscarFavoritoUser(Utils.getCurrentUserSession(), 
                    this.producto)){
                FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "El producto "+this.producto.getTitulo()+" "
                                + "ya se encuentra en su lista de favoritos."));
            } else {
        
                // Seteamos el producto a la lista de favoritos
                this.favorito.setIdProducto(this.producto);

                // Seteamos el usuario a la lista de favoritos
                this.favorito.setIdUsuario(Utils.getCurrentUserSession());
                               
                try {

                // Insertamos en la BDD
                this.favoritoEJB.create(this.favorito);

                 FacesContext.getCurrentInstance().addMessage("messages",
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Se ha guardado "+this.producto.getTitulo()+" "
                                    + "en la lista de favoritos."));
                 
                 } catch (Exception e) {
        
                    System.out.println("Error añadiendo a favoritos: "+ e.getMessage());

                }
            }
            
      
    }
     
}
