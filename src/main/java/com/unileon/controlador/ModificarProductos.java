/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.ProductoFacadeLocal;
import com.unileon.modelo.Producto;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author eduardojuarezrobles
 */
@Named
@ViewScoped
public class ModificarProductos implements Serializable{
    
    @EJB
    private ProductoFacadeLocal productoEJB;
    
    @Inject
    private Producto producto;
    
    //lista de productos        
    List<Producto> listaProductos;
    
    @PostConstruct
    public void inicio(){
        
       this.listaProductos=this.productoEJB.findAll(); 
       
   }
    
    // Getters & setters
    public ProductoFacadeLocal getProductoEJB() {
        return productoEJB;
    }

    public void setProductoEJB(ProductoFacadeLocal productoEJB) {
        this.productoEJB = productoEJB;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
   
    
    
    /**
     * Establecer el producto que se ha seleccionado en la tabla 
     * @param prod 
     */
    public void productoSeleccionado(Producto prod){
        this.producto=prod;
    }
    
    
   /**
    * Metodo que modifica los productos
    */
   public void modificarProductos(){
       
       try{
           System.out.println(this.producto+"me cago es su puta madre");
           this.productoEJB.edit(this.producto);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.producto.getTitulo()
                            +" actualizado con éxito. "));
           
       }catch (Exception e){
           
           FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modificando los datos de "
                            + this.producto.getTitulo()));
           
           System.out.println("Error al modificar los productos: "+e.getMessage());
           
       }
   }
   
   
   public void eliminarProductos(){
       
        try {
           FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.producto.getTitulo()
                                        +" eliminada con éxito!. "));
            this.productoEJB.remove(this.producto);
            
         
        } catch (Exception e) {
        
            // Tratamiento del error
            System.out.println("Error al eliminar el producto: "+e.getMessage());
            
        }
       
   }
   
}
