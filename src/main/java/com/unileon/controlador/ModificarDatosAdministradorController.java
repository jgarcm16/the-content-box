/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.PersonaFacadeLocal;
import com.unileon.EJB.RolFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Persona;
import com.unileon.modelo.Rol;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author eduardojuarezrobles
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class ModificarDatosAdministradorController implements Serializable{
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    @Inject
    private Usuario usuario;
    
    @EJB
    private PersonaFacadeLocal personaEJB;
    
    @Inject
    private Persona persona;
    
    @EJB
    private RolFacadeLocal rolEJB;
    
    @Inject
    private Rol rol;
    
    private String nuevoUser;
    private String contraseña;
    private String repiteContraseña;
    
    private List<Usuario> listaUsuarios;
    private List<Rol> listaRoles;

    
    @PostConstruct
    public void inicio(){
    
        this.listaUsuarios = this.usuarioEJB.findAll();
        this.listaRoles = this.rolEJB.findAll();
        
        this.repiteContraseña = "";
    }
    
    // Getters & setters
    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }
    
    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }
    
    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }
    
    public PersonaFacadeLocal getPersonaEJB() {
        return personaEJB;
    }
    public void setPersonaEJB(PersonaFacadeLocal personaEJB) {
        this.personaEJB = personaEJB;
    }
    public Persona getPersona() {
        return persona;
    }
    
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getRepiteContraseña() {
        return repiteContraseña;
    }

    public void setRepiteContraseña(String repiteContraseña) {
        this.repiteContraseña = repiteContraseña;
    }

    public String getNuevoUser() {
        return nuevoUser;
    }

    public void setNuevoUser(String nuevoUser) {
        this.nuevoUser = nuevoUser;
    }
    
    public RolFacadeLocal getRolEJB() {
        return rolEJB;
    }

    public void setRolEJB(RolFacadeLocal rolEJB) {
        this.rolEJB = rolEJB;
    }
    
    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    public List<Rol> getListaRoles() {
        return listaRoles;
    }

    public void setListaRoles(List<Rol> listaRoles) {
        this.listaRoles = listaRoles;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    
    
    /**
     * Establecer el usuario que se ha selecionado en la tabla
     * @param usr 
     */
    public void establecerUsuarioModificar(Usuario usr){
    
        this.usuario = usr;
        this.persona = this.usuario.getIdPersona();
        this.nuevoUser = this.usuario.getUsername();
   
    }
   
    public void modificarPersona(){
    
        try {
            
            this.personaEJB.edit(this.persona);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.usuario.getUsername()
                            +" actualizada con éxito. "));
        
        } catch (Exception e) {
        
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modificando los datos de "
                            + this.usuario.getUsername()));
            
            System.out.println("Error al modificar datos de persona: " + e.getMessage());
        
        }  
    }
    
    public void modificarUsuario(){
    
        try { 
            
            // Si las contraseñas coinciden o no se ha cambiado la contraseña
            if (this.repiteContraseña.equals(this.usuario.getPassword()) || this.contraseña.length() == 0){
                
                //si el rol no se ha modificado
                if(this.usuario.getRol().getIdRol() != this.rol.getIdRol()){
                    
                    // Seteamos el rol del usuario
                    this.usuario.setRol(this.rol);
                    System.out.println("Le he puesto "+ this.rol.getIdRol());
                    
                }
                    
                    
                    // El usuario no ha modificado el nombre de usuario
                    if (this.usuario.getUsername().equals(this.nuevoUser)){

                        this.usuarioEJB.edit(this.usuario);   
                        FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.usuario.getUsername()
                                    +" actualizada con éxito. "));

                    // El usuario ha modificado el nombre de usuario
                    } else {

                        // Comprobamos que el nombre elegido no existe en la lista de usuarios
                        boolean existe = false;

                        for (int i = 0; i < this.listaUsuarios.size(); i++){
                            if (this.nuevoUser.equals(this.listaUsuarios.get(i).getUsername()))
                                existe = true;
                        }
                         // El usuario elegido existe en la base de datos y no se puede actualizar
                        if (existe){

                             FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El nombre de usuario ya está registrado en la base"
                                        + "de datos."));

                        // El usuario elegido no existe en la base de datos -> actualizamos los datos del usuarios     
                        } else {
                                // Seteamos el nombre de usuario al usuario
                                this.usuario.setUsername(this.nuevoUser);

                                this.usuarioEJB.edit(this.usuario);   
                                FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.usuario.getUsername()
                                        +" actualizada con éxito!. "));

                                // Actualizamos el valor del usuario selecionado
                                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario",this.usuario);
                        }
                    }
               // }
                
            } else {
            
                FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Las contraseñas deben coincidir."));
            
            }
        
        } catch (Exception e) {
        
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modificando los datos de "
                            + this.usuario.getUsername()));
            
            System.out.println("Error al modificar datos de usuario: " + e.getMessage());
        
        }       
    }
    
    public void eliminarUsuario(){
        
        try {
           
            this.usuarioEJB.remove(this.usuario);
            this.personaEJB.remove(this.persona);
         
        } catch (Exception e) {
        
            // Tratamiento del error
            System.out.println("Error al eliminar al usuario: "+e.getMessage());
            
        }
        
    }
    
    /*//funcion por si se hace por separado la modificacion de permisos
    public void ModificarPermisos(){
        
         try {
             
            this.usuario.setRol(this.nuevoRol);
            this.usuarioEJB.edit(this.usuario);  
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Información de "+this.usuario.getUsername()
                            +" actualizada con éxito. "));
         
        } catch (Exception e) {
            // Tratamiento del error
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error modificando los datos de "
                            + this.usuario.getUsername()));
            System.out.println("Error al cambiar los permisos al usuario: "+e.getMessage());   
        }
    }*/
}
