/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.MenuFacadeLocal;
import com.unileon.modelo.Menu;
import com.unileon.modelo.TipoMenu;
import com.unileon.modelo.Usuario;
import com.unileon.utils.Utils;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author jorgmateos
 */

// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@SessionScoped
public class MenuController implements Serializable{
    
    @EJB
    private MenuFacadeLocal menuEJB;
    
    // Atributo del controlador que se encargará del init
    private MenuModel modelo;
    
    @PostConstruct
    public void inicio(){
            
        this.modelo = this.obtenerMenu();
        
    }
    
    // Getters & setters
    public MenuFacadeLocal getMenuEJB() {
        return menuEJB;
    }

    public void setMenuEJB(MenuFacadeLocal menuEJB) {
        this.menuEJB = menuEJB;
    }

    public MenuModel getModelo() {
        return modelo;
    }

    public void setModelo(MenuModel modelo) {
        this.modelo = modelo;
    }

    public MenuModel obtenerMenu() {
        
        // Incicializamos el menu
        MenuModel result = new DefaultMenuModel();
        
        
        // Obtenemos el usuario que está en la sesión actual
        Usuario user = Utils.getCurrentUserSession();

       // Obtenemos la lista de menús pertenecientes al usuario
       List<Menu> listaMenus = this.menuEJB.getListaMenusUser(user);
       
        for (int i = 0; i < listaMenus.size(); i++) {
            
            // Si es SUBMENÚ no tiene padre -> padre = null pero sí tiene
            // hijos
            if (listaMenus.get(i).getTipo().equals(TipoMenu.S)) {
            
                // Creamos el submenú con el nombre
                DefaultSubMenu subMenu = new DefaultSubMenu(listaMenus.get(i).getNombre());

                // Buscamos los hijos del submenú
                for (int j = 0; j < listaMenus.size(); j++){
                
                    // Para que sea un hijo, su padre no puede ser NULL
                    // Además, tiene que ser de tipo ITEM y tener el mismo id del padre
                    if (listaMenus.get(j).getIdPadre()!= null){
                        
                        if ((listaMenus.get(j).getTipo().equals(TipoMenu.I)) && 
                                (listaMenus.get(j).getIdPadre().getIdMenu() == listaMenus.get(i).getIdMenu())){
                        
                            // Creamos el ITEM
                            DefaultMenuItem itemSubmenu = new DefaultMenuItem(listaMenus.get(j).getNombre());
                            
                            // Le añadimos el URL del padre
                            itemSubmenu.setUrl("/TheContentBox/faces"+listaMenus.get(j).getUrl()+".xhtml?faces-redirect=true");
                            
                            // Lo añadimos al submenú
                            subMenu.addElement(itemSubmenu);
                            
                        }
                            
                    }
                    
                }
                
                // Añadimos el submenú al modelo
                result.addElement(subMenu);
                
            // Si es ITEM puede no tener padre (como si fuera un link)
            // o tenerlo (padre = idPadre) -> funciona como item de un submenú
            } else if (listaMenus.get(i).getTipo().equals(TipoMenu.I) &&
                    listaMenus.get(i).getIdPadre()== null) {
            
                // Creamos el Item del menú que funcionará como un link
                DefaultMenuItem itemMenu = new DefaultMenuItem(listaMenus.get(i).getNombre());
                
                // Le añadimos el URL del padre
                itemMenu.setUrl("/TheContentBox/faces/"+listaMenus.get(i).getUrl()+".xhtml?faces-redirect=true");
                
                // Añadimos el item al resultado
                result.addElement(itemMenu);
            }
            
        }
        
        
        // Para p:menuBar
        result.generateUniqueIds();
       
        return result;
        
    }
    
    public String destruirSesion() throws IOException{
        String direccion = "/faces/index";
                
        // Cerramos sesión
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        
        // Devolvemos el índice
        return direccion;
    }
     
}
