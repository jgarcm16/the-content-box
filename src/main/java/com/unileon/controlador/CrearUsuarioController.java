/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.modelo.Usuario;
import com.unileon.EJB.PersonaFacadeLocal;
import com.unileon.EJB.RolFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.Persona;
import com.unileon.modelo.Rol;
import com.unileon.modelo.Usuario;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import static org.eclipse.persistence.expressions.ExpressionOperator.currentDate;

/**
 *
 * @author jorgmateos
 */
// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class CrearUsuarioController implements Serializable{
    
   @EJB
   private UsuarioFacadeLocal usuarioEJB;
   
   @EJB
   private RolFacadeLocal rolEJB;
   
   @EJB
   private PersonaFacadeLocal personaEJB;
   
   private String contrasena1;
   private String contrasena2;
   
   @Inject
   private Usuario usuario;
   @Inject
   private Persona persona;
   @Inject
   private Rol rol;
   
   private List<Usuario> listaUsuarios;
   private List<Rol> listaRoles;
   private List<Persona> listaPersonas;
   
   @PostConstruct
   public void inicio(){
       
       this.contrasena1 = "";
       this.contrasena2 = "";
   
        this.listaUsuarios = this.usuarioEJB.findAll();
        this.listaRoles = this.rolEJB.findAll();
        this.listaPersonas = this.personaEJB.findAll();
   
   }
   
   // Getters & setters
    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public RolFacadeLocal getRolEJB() {
        return rolEJB;
    }

    public void setRolEJB(RolFacadeLocal rolEJB) {
        this.rolEJB = rolEJB;
    }
    public PersonaFacadeLocal getPersonaEJB() {
        return personaEJB;
    }

    public void setPersonaEJB(PersonaFacadeLocal personaEJB) {
        this.personaEJB = personaEJB;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<Rol> getListaRoles() {
        return listaRoles;
    }

    public void setListaRoles(List<Rol> listaRoles) {
        this.listaRoles = listaRoles;
    }
    
    public List<Persona> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(List<Persona> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }

    public String getContrasena1() {
        return contrasena1;
    }

    public void setContrasena1(String contrasena1) {
        this.contrasena1 = contrasena1;
    }

    public String getContrasena2() {
        return contrasena2;
    }

    public void setContrasena2(String contrasena2) {
        this.contrasena2 = contrasena2;
    }
    
    
    public void crearUsuario(){
        
        // Comprobamos que las contraseñas coinciden
        if (this.contrasena1.equals(this.contrasena2)){
            
            // Asignamos la contraseña al usuario
            this.usuario.setPassword(contrasena1);
        
            // Asignamos el rol al usuario
            this.usuario.setRol(this.rol);

            // Asignamos la persona al usuario
            this.usuario.setIdPersona(this.persona);

            // Asignamos el saldo inicial
            this.usuario.setSaldo(100);

            // Comrpobamos la edad del usuario

            // Obtenemos fecha de nacimiento del usuario
            Date fechaNacimiento = this.persona.getFechaNacimiento();
            // Obtenemos fecha actual
            Date currentDate = new Date();

            // Parseamos las fechas
            DateFormat formatter = new SimpleDateFormat("yyyyMMdd");                           
            int nacimiento = Integer.parseInt(formatter.format(fechaNacimiento));                            
            int actual = Integer.parseInt(formatter.format(currentDate));

            // Calculamos la edad
            int age = (actual - nacimiento) / 10000;                                                       

            if (age < 18){
                 FacesContext.getCurrentInstance().addMessage("fechaNacimiento",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario debe ser mayor de edad."));
            } else {

                // Comprobamos si el usuario existe en la base de datos
                String username = this.usuario.getUsername();
                boolean existe = false;

                for (int i = 0; i < this.listaUsuarios.size(); i++){

                    if (username.equals(this.listaUsuarios.get(i).getUsername()))
                        existe = true;

                }

                if (existe){
                     FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario ya existe"));
                } else {
                    // Creamos el usuario
                    try {
                    
                        this.usuarioEJB.create(this.usuario);
                        
                        this.inicio();

                         FacesContext.getCurrentInstance().addMessage("messages",
                                new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Usuario creado con éxito"));
                    
                    } catch (Exception e) {
                    
                        System.out.println("Error al crear el usuario: "+e.getMessage());
                    
                    }
                }

            }
        } else {
        
             FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Las contraseñas deben coincidir"));
            
        }
    }
    
    public void registrarUsuario(){
        
        // Comprobamos que las contraseñas coinciden
        if (this.contrasena1.equals(this.contrasena2)){
            
            // Asignamos la contraseña al usuario
            this.usuario.setPassword(contrasena1);
        
            // Asignamos el rol al usuario
            this.usuario.setRol(this.rolEJB.find(1));

            // Asignamos la persona al usuario
            this.usuario.setIdPersona(this.persona);

             // Asignamos el saldo inicial
            this.usuario.setSaldo(100);

            // Comprobamos la edad del usuario
            // Obtenemos fecha de nacimiento del usuario
            Date fechaNacimiento = this.persona.getFechaNacimiento();
            // Obtenemos fecha actual
            Date currentDate = new Date();

            // Parseamos las fechas
            DateFormat formatter = new SimpleDateFormat("yyyyMMdd");                           
            int nacimiento = Integer.parseInt(formatter.format(fechaNacimiento));                            
            int actual = Integer.parseInt(formatter.format(currentDate));

            // Calculamos la edad
            int age = (actual - nacimiento) / 10000;                                                       

            if (age < 18){
                 FacesContext.getCurrentInstance().addMessage("fechaNacimiento",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario debe ser mayor de edad."));
            } else {

                // Comprobamos si el usuario existe en la base de datos
                String username = this.usuario.getUsername();
                boolean existe = false;

                for (int i = 0; i < this.listaUsuarios.size(); i++){

                    if (username.equals(this.listaUsuarios.get(i).getUsername()))
                        existe = true;

                }

                if (existe){
                     FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario ya existe"));
                } else {
                    // Creamos el usuario
                    try {
                    
                        this.usuarioEJB.create(this.usuario);
                        
                        this.inicio();

                         FacesContext.getCurrentInstance().addMessage("messages",
                                new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Usuario creado con éxito"));
                    
                    } catch (Exception e) {
                    
                        System.out.println("Error al crear el usuario: "+e.getMessage());
                    
                    }
                }
            }
            
        } else {
        
             FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Las contraseñas deben coincidir"));
            
        }
    }
    
}
