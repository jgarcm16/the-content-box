/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.controlador;

import com.unileon.EJB.MisProductosFacadeLocal;
import com.unileon.EJB.UsuarioFacadeLocal;
import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author eduardojuarezrobles
 */

// Para que pueda ser vista desde el html.
// Si ni ponemos nombre en @Named accederemos a ella de la misma forma pero empezando x minúscula.
@Named
@ViewScoped
public class IndexController implements Serializable{
    
    @EJB
    private UsuarioFacadeLocal usuarioEJB;
    
    @EJB
    private MisProductosFacadeLocal misProductosEJB;
    
    @Inject
    private Usuario usuario;
    
    public String verificarUsuario(){
        
        try {
            
            Usuario result = this.usuarioEJB.verificarUsuario(this.usuario);

            // Algo salió mal en el inicio de sesión
            if (result == null){
                
                // Comprobamos si el usuario existe en el sistema
                // Si existe, quiere decir que ha introducido mal la contraseña y que debemos
                // incrementar su contador de inicio de sesión
                result = this.usuarioEJB.buscarUsername(this.usuario.getUsername());
                
                // Quiere decir que existe un usuario pero que ha introducido la contraseña mal
                // Incrementamos el contador
                if (result != null){
                    
                    // Obtenemos los intentos del usuario
                    int numIntentos = result.getcontadorInicioSesion();
                    
                    // Los sumamos
                    numIntentos++;
                    
                    // Los seteamos al usuario
                    result.setContadorInicioSesion(numIntentos);
                    
                    // Comoprobamos que no supera el límite
                    if (numIntentos >= 5){
                        
                        // Setteamos a true el estado de usuario para bloquearlo
                        result.setEstado(true);
                        
                        // Informamos al usuario  
                        FacesContext.getCurrentInstance().addMessage("messages",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario ha excedido el número de intentos"
                                    + "de inicio de sesión. Su cuenta ha sido bloqueada. Por favor, póngase en contacto con el"
                                    + "administrador del sistema."));
                        
                    // No ha llegado al límite pero le quedan pocos intentos
                    } else {
                        
                        // Informamos al usuario  
                        FacesContext.getCurrentInstance().addMessage("messages",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Contraseña incorrecta. Quedan " + (5 - numIntentos) +""
                                    + " intentos hasta que su cuenta quede bloqueada." ));
                        
                    }
                    
                    // Lo guardamos en la base de datos
                    this.usuarioEJB.edit(result);
                    
                    return "";
  
                }
                
                FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario no se encuentra registrado en"
                                + "el sistema. Para acceder, regístrese."));
                return "";
               
            // Contraseña y usuario introducidos correctamente    
            } else {
                
                // Comprobamos si el usuario no está bloqueado
                boolean estado = result.isEstado();
                
                
                if (estado){
                    FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El usuario fue bloqueado. Por favor, "
                                + "póngase en contacto con el administrador del sistema."));
                    return "";
                
                // Si no está bloqueado, comprobamos el número de intentos que lleva de inicio de sesión
                } else {
                    
                    int numIntentos = result.getcontadorInicioSesion();
                    
                    if (numIntentos >= 5){
                        FacesContext.getCurrentInstance().addMessage("messages",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se ha excedido el número de intentos. "
                                    + "La cuenta estña bloqueada. Por favor, "
                                    + "póngase en contacto con el administrador del sistema."));
                        return "";

                        
                    // Si no, hacemos login
                    } else {
                        
                        // Reseteamos el contador de inicio de sesión
                        result.setContadorInicioSesion(0);
                                                
                        // Lo guardamos en la base de datos
                        this.usuarioEJB.edit(result);
                        
                        // Comprobamos y modificamos si es preciso la lista de productos propios del usuario
                        List<MisProductos> listaProductosUser = this.misProductosEJB.obtenerProductosUsuario(result);
                        this.updateMisProductosUsuario(listaProductosUser, result);
                        
                        // Iniciamos sesión con el usuario y lo guardamos en las variables de sesión
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario",result);
                        
                        //return "private/principal.xhtml?faces-redirect=true";
                        return "private/usuario/escribirMensaje.xhtml?faces-redirect=true";
                   
                    }
                
                }
            }
            
        } catch (Exception e) {
                
            // Tratamiento del error
            System.out.println("Error al verificar usuario: "+e.getMessage());
            return null;
            
        }
    }
    public String registrarUsuario() throws IOException{
        // Devolvemos la pagina de registro
        return "registro.xhtml?faces-redirect=true";
    }
    
    /**
     * Método que comprueba si los productos del usuario están caducados y en caso de estarlo, modifica
     * sus respectivos valores en la BDD
     * @param listaProductosUser
     * @param result 
     */
    private void updateMisProductosUsuario(List<MisProductos> listaProductosUser, Usuario result) {

        for (int i = 0; i < listaProductosUser.size(); i++){
        
            // Si es alquilado, comprobamos la fecha de alquiler
            if (listaProductosUser.get(i).isAlquilado()){
            
                Date fechaExpiracion = listaProductosUser.get(i).getFechaExpiracion();
                Date fechaActual = new Date();
                
               // Comprobamos si la fecha de expiración es posterior a la fecha actual  
               if(fechaExpiracion.before(fechaActual)){
               
                   // Si entramos aquí quiere decir que el producto está caducado -> modificamos
                   // el estado del producto
                   if (listaProductosUser.get(i).isEstado()){
                       
                       // Modificamos el estado si no lo estaba
                       listaProductosUser.get(i).setEstado(false);
                       this.misProductosEJB.edit(listaProductosUser.get(i));
                   }
               
               }
                 
                
            }
            
        }

    }
    
    // Getters & setters
    public UsuarioFacadeLocal getUsuarioEJB() {
        return usuarioEJB;
    }

    public void setUsuarioEJB(UsuarioFacadeLocal usuarioEJB) {
        this.usuarioEJB = usuarioEJB;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    } 

    
}
