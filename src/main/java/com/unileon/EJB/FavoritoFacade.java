/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Favorito;
import com.unileon.modelo.Producto;
import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jorgmateos
 */
@Stateless
public class FavoritoFacade extends AbstractFacade<Favorito> implements FavoritoFacadeLocal {

    @PersistenceContext(unitName = "com.unileon_TheContentBox_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FavoritoFacade() {
        super(Favorito.class);
    }

    @Override
    public boolean buscarFavoritoUser(Usuario idUsuario, Producto idProducto) {
        
        String consulta = 
                "FROM Favorito f "
                + "WHERE "
                + "f.idUsuario=:param1 "
                + "and "
                + "f.idProducto=:param2";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", idUsuario); 
        query.setParameter("param2", idProducto); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<Favorito> resultado = query.getResultList();
               
        if (resultado.size() == 0)
            return false;
        else 
            return true;
        
    }

    @Override
    public List<Favorito> obtenerFavoritosUser(Usuario currentUserSession) {

        String consulta = 
                "FROM Favorito f "
                + "WHERE "
                + "f.idUsuario=:param1";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", currentUserSession); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<Favorito> resultado = query.getResultList();
               
        return resultado;

    }
    
}
