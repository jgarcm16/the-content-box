/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.CatNoticias;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface CatNoticiasFacadeLocal {

    void create(CatNoticias catNoticias);

    void edit(CatNoticias catNoticias);

    void remove(CatNoticias catNoticias);

    CatNoticias find(Object id);

    List<CatNoticias> findAll();

    List<CatNoticias> findRange(int[] range);

    int count();
    
}
