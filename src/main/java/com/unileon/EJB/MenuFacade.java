/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Menu;
import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jorgmateos
 */
@Stateless
public class MenuFacade extends AbstractFacade<Menu> implements MenuFacadeLocal {

    @PersistenceContext(unitName = "com.unileon_TheContentBox_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuFacade() {
        super(Menu.class);
    }
    
    @Override
    public List<Menu> getListaMenusUser(Usuario user) {
        
        List<Menu> listaResult = null;
        
        // Obtenemos el rol del usuario
        int idRol = user.getRol().getIdRol();
        
        // Realizamos la consulta
        try {
            
            String consulta = ""
                    + "FROM Menu m "
                    + "WHERE m.idRol.idRol = :rol";
            
            Query oQuery = em.createQuery(consulta);
            oQuery.setParameter("rol", idRol);
            
            listaResult = oQuery.getResultList();
        
        } catch (Exception e) {
        
            System.out.println("Error en obtener menus usuario: "+e.getMessage());
            return null;
            
        }
        
        return listaResult;
        
    }
    
}
