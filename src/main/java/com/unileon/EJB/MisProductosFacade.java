/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Favorito;
import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Producto;
import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jorgmateos
 */
@Stateless
public class MisProductosFacade extends AbstractFacade<MisProductos> implements MisProductosFacadeLocal {

    @PersistenceContext(unitName = "com.unileon_TheContentBox_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MisProductosFacade() {
        super(MisProductos.class);
    }

    /**
     * Método que comprueba si un producto está en la lista de misProductos del usuario
     * @param currentUserSession
     * @param producto
     * @return 
     */
    @Override
    public boolean isMisProductos(Usuario user, Producto producto) {

        String consulta = 
                "FROM MisProductos m "
                + "WHERE "
                + "m.idUsuario=:param1 "
                + "and "
                + "m.idProducto=:param2";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", user); 
        query.setParameter("param2", producto); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<MisProductos> resultado = query.getResultList();
               
        if (resultado.size() == 0)
            return false;
        else 
            return true;
        
    }

    /**
     * Método que devuelve una lista de procutos con los productosq
     * que el usuario pasado como parámetro tiene en MisProductos
     * @param user
     * @return 
     */
    @Override
    public List<MisProductos> obtenerProductosUsuario(Usuario user) {
        
        String consulta = 
                "FROM MisProductos m "
                + "WHERE "
                + "m.idUsuario=:param1";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", user); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<MisProductos> resultado = query.getResultList();
               
        return resultado;
        
    }
    
}
