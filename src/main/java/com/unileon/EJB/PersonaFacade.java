/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Persona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author eduardojuarezrobles
 */
@Stateless
public class PersonaFacade extends AbstractFacade<Persona> implements PersonaFacadeLocal {

    @PersistenceContext(unitName = "com.unileon_TheContentBox_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonaFacade() {
        super(Persona.class);
    }
    
    @Override
    public Persona buscarNombre(String nombre) {
        
        String consulta = 
                "FROM Persona p "
                + "WHERE "
                + "p.nombre=:param1 ";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", nombre); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<Persona> resultado = query.getResultList();
        
        /* La consulta nos devuelve una lista de tipo Usuario 
        o un valor nulo si no hay coincidencia en base de datos 
        con los valores enviados. Según el resultado obtenido, 
        devolveremos un nulo en caso de que éste sea nulo, y un 
        objeto de tipo Usuario en caso de que la consulta nos haya 
        devuelto algún resultado. */
        if (resultado.size() == 0)
            return null;
        else return resultado.get(0);
        
    }
    
}
