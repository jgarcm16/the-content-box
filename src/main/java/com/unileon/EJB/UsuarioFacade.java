/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author eduardojuarezrobles
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "com.unileon_TheContentBox_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario verificarUsuario(Usuario us) {
        
        String consulta = 
                "FROM Usuario u "
                + "WHERE "
                + "u.username=:param1 "
                + "and "
                + "u.password=:param2";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", us.getUsername()); 
        query.setParameter("param2", us.getPassword()); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<Usuario> resultado = query.getResultList();
               
        /* La consulta nos devuelve una lista de tipo Usuario 
        o un valor nulo si no hay coincidencia en base de datos 
        con los valores enviados. Según el resultado obtenido, 
        devolveremos un nulo en caso de que éste sea nulo, y un 
        objeto de tipo Usuario en caso de que la consulta nos haya 
        devuelto algún resultado. */
        if (resultado.size() == 0)
            return null;
        else return resultado.get(0);
        
    }

    @Override
    public Usuario buscarUsername(String username) {
        
        String consulta = 
                "FROM Usuario u "
                + "WHERE "
                + "u.username=:param1 ";
        
        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", username); 
        
        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<Usuario> resultado = query.getResultList();
        
        /* La consulta nos devuelve una lista de tipo Usuario 
        o un valor nulo si no hay coincidencia en base de datos 
        con los valores enviados. Según el resultado obtenido, 
        devolveremos un nulo en caso de que éste sea nulo, y un 
        objeto de tipo Usuario en caso de que la consulta nos haya 
        devuelto algún resultado. */
        if (resultado.size() == 0)
            return null;
        else return resultado.get(0);
        
    }
    
}

