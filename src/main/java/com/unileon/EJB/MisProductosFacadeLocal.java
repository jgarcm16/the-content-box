/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Producto;
import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jorgmateos
 */
@Local
public interface MisProductosFacadeLocal {

    void create(MisProductos misProductos);

    void edit(MisProductos misProductos);

    void remove(MisProductos misProductos);

    MisProductos find(Object id);

    List<MisProductos> findAll();

    List<MisProductos> findRange(int[] range);

    int count();

    // Métodos propios
    public boolean isMisProductos(Usuario currentUserSession, Producto producto);

    public List<MisProductos> obtenerProductosUsuario(Usuario currentUserSession);
    
}
