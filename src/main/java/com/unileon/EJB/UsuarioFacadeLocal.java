/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author eduardojuarezrobles
 */
@Local
public interface UsuarioFacadeLocal {

    void create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    Usuario find(Object id);

    List<Usuario> findAll();

    List<Usuario> findRange(int[] range);

    int count();
    
    // Métodos propios

    /**
     *
     * @param us
     * @return
     */
    Usuario verificarUsuario(Usuario us);

    /**
     * 
     * @param username
     * @return 
     */
    public Usuario buscarUsername(String username);
    
}
