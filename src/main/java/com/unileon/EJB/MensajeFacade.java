/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Mensaje;
import com.unileon.modelo.MisProductos;
import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jorgmateos
 */
@Stateless
public class MensajeFacade extends AbstractFacade<Mensaje> implements MensajeFacadeLocal {

    @PersistenceContext(unitName = "com.unileon_TheContentBox_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MensajeFacade() {
        super(Mensaje.class);
    }

    @Override
    public List<Mensaje> obtenerMensajesUsuario(Usuario currentUserSession) {
        
        String consulta = 
                "FROM Mensaje m "
                + "WHERE "
                + "m.receptor=:param1";

        Query query = em.createQuery(consulta);
        // La consulta ha sido creada, damos valor a cada prámetro:
        query.setParameter("param1", currentUserSession); 

        // Ejecutamos la sentencia y obtenemos el resultado: 
        List<Mensaje> resultado = query.getResultList();

        return resultado;
        
    }
    
}

