/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.EJB;

import com.unileon.modelo.Favorito;
import com.unileon.modelo.Producto;
import com.unileon.modelo.Usuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jorgmateos
 */
@Local
public interface FavoritoFacadeLocal {

    void create(Favorito favorito);

    void edit(Favorito favorito);

    void remove(Favorito favorito);

    Favorito find(Object id);

    List<Favorito> findAll();

    List<Favorito> findRange(int[] range);

    int count();

    // Métodos propios
    
    /**
     * Método que comprueba si el producto que el usuario selecciona se encuentra ya
     * entre sus productos favoritos o no
     * @param idUsuario
     * @param idProducto
     * @return true si el producto ya está en la lista de favoritos del usuario
     * false si el producto no está en la lista de favoritos del usuario
     */
    public boolean buscarFavoritoUser(Usuario idUsuario, Producto idProducto);

    
    /**
     * Método que devuelve la lista de elementos favoritos que tiene el usuario
     * pasado como parámetro
     * @param currentUserSession
     * @return lista de productos favoritos que tiene el usuario
     */
    public List<Favorito> obtenerFavoritosUser(Usuario currentUserSession);
    
}
