/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unileon.utils;

import com.unileon.modelo.Usuario;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import javax.faces.context.FacesContext;

/**
 *
 * @author jorgmateos
 */
public class Utils {
    
    /**
     * Método que devuelve el usuario que se encuentra actualmente
     * en la sesión
     * @return Usuario user - usuario que se encuentra actualmente
     * en la sesión
     */
    public static Usuario getCurrentUserSession(){
    
        // Creamos el usuario
        Usuario user = new Usuario();
        
        // Obtenemos el usuario de la sesión actual
        user = (Usuario) (Usuario) FacesContext.getCurrentInstance()
                .getExternalContext().getSessionMap().get("usuario");
        
        // Lo devolvemos
        return user;
        
    }
    
    /**
     * Método para calcular la fecha en la que el producto expira
     * @param días
     * @return 
     */
    public static Date calcularFecha(int dias) {

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date()); // Configuramos la fecha que se recibe
      calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
      
      return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }
    
    /**
     * Método para calcular los días entre dos fechas
     * @param fecha1
     * @param fecha2
     * @return 
     */
    public static long calcularDias(Date fecha1, Date fecha2) {
        
        LocalDate date1 = convertToLocalDateViaInstant(fecha1);
        LocalDate date2 = convertToLocalDateViaInstant(fecha2);
        
        return ChronoUnit.DAYS.between(date1, date2);

    }
    
    private static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
          .atZone(ZoneId.systemDefault())
          .toLocalDate();
    }
    
    public static String stripAccents(String s) {
        
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
        
    }
    
    // También se puede utilizar este paquete para crear métodos de 
    // cerrar sesión, etc. todos los que utilicden FacesContext

    public static String fechaFormateada(Date fecha) {

        String strDateFormat = "dd-MMM-yyyy"; // El formato de fecha está especificado  
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
        
        return objSDF.format(fecha);
    }

    
    
}
