package com.unileon.modelo;

import com.unileon.modelo.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-30T10:27:25")
@StaticMetamodel(Mensaje.class)
public class Mensaje_ { 

    public static volatile SingularAttribute<Mensaje, Integer> idMensaje;
    public static volatile SingularAttribute<Mensaje, Date> fecha;
    public static volatile SingularAttribute<Mensaje, String> contenido;
    public static volatile SingularAttribute<Mensaje, Boolean> estado;
    public static volatile SingularAttribute<Mensaje, Usuario> receptor;
    public static volatile SingularAttribute<Mensaje, String> asunto;
    public static volatile SingularAttribute<Mensaje, Usuario> emisor;

}